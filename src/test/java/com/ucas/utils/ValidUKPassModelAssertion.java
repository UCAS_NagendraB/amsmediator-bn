package com.ucas.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.mule.api.MuleEvent;
import org.mule.munit.assertion.MunitAssertion;

import com.ucas.ukpass.model.Address;
import com.ucas.ukpass.model.Applicant;

public class ValidUKPassModelAssertion implements MunitAssertion {

    private static String FLOWVAR_NAME = "applicant";
    
    //Check if the Dataweave has populated all necessary fields for UKPass/Topaz
    @Override
    public MuleEvent execute(MuleEvent muleEvent) throws AssertionError {
        Applicant applicant = (Applicant) muleEvent.getFlowVariable(FLOWVAR_NAME);
        if (!applicant.getCriminalConvictionFlagAsString().equals("Y")) {
            throwAssertion("criminalConvictionFlag should have the value \"Y\"");
        }
        Date validDateOfBirth = convertDate("1991-12-11T00:00:00T");
        if (!applicant.getDateOfBirth().equals(validDateOfBirth)) {
            throwAssertion("dateOfBirth should have the value of December 1991");
        }
        Date validDateUKEntry = convertDate("2011-07-25T12:34:43.913Z");
        if (!applicant.getDateUKEntry().equals(validDateUKEntry)) {
            throwAssertion("dateUKEntry should have the value of August 2011");
        }
        if (!applicant.getEmailAddress().equals("supermarine@mailinator.com")) {
            throwAssertion("emailAddress should have the value of \"supermarine@mailinator.com\"");
        }
        if (!applicant.getEmailAddressVerifiedFlag().equals('Y')) {
            throwAssertion("emailAddressVerifiedFlag should have the value 'Y'");
        }
        if (!applicant.getForenames().equals("Pepe")) {
            throwAssertion("fornames should have the value 'Pepe'");
        }
        if (!applicant.getHomeTelephone().equals("0123456789")) {
            throwAssertion("homeTelephone should have the value of '0123456789'");
        }
        if (!applicant.getLargeFontRequiredFlag().equals('N')) {
            throwAssertion("largeFontRequiredFlag should have the value of 'N'");
        }
        if (!applicant.getMobileTelephone().equals("1234567891")) {
            throwAssertion("mobileTelephone should have the value of '1234567891'");
        }
        if (!applicant.getPassportNumber().equals("AB123456")) {
            throwAssertion("passportNumber should have the value of 'AB123456'");
        }
        Date validPassportIssueDate = convertDate("2017-07-25T12:34:43.913Z");
        if (!applicant.getPassportIssueDate().equals(validPassportIssueDate)) {
            throwAssertion("passportIssueDate should have the value of August 2017");
        }
        Date validPassportExpirydate = convertDate("2018-07-25T12:34:43.913Z");
        if (!applicant.getPassportExpiryDate().equals(validPassportExpirydate)) {
            throwAssertion("passportExpiryDate should have the value of August 2018");
        }
        if (!applicant.getPassportPlaceOfIssue().equals("PlaceOfIssue")) {
            throwAssertion("passportPlaceOfIssue should have the value of 'PlaceOfIssue'");
        }
        if (!applicant.getPermanentHomeUk().toString().equals("Y")) {
            throwAssertion("permanentHomeUk should have the value 'Y'.");
        }
        if (!applicant.getPersonalId().equals("0000000110")) {
            throwAssertion("personalId should have the value 0000000110.");
        }
        if (!applicant.getPreferredFirstName().equals("PreferredName")) {
            throwAssertion("preferredFirstName should have the value of 'PreferredName'");
        }
        if (!applicant.getPreviousSurname().equals("PreviousNameK")) {
            throwAssertion("previousSurname should have the value of 'PreviousNameK'");
        }
        if (!applicant.getSex().toString().equals("O")) {
            throwAssertion("sex should have the value 'O'");
        }
        if (!applicant.getSpecialNeeds().equals("I like computers too much")) {
            throwAssertion("specialNeeds should have the value \"I like computers too much\"");
        }
        if (!applicant.getStudentVisaRequired().equals("N")) {
            throwAssertion("studentVisaRequired should have the value \"M\"");
        }
        if (!applicant.getSurname().equals("Moreno")) {
            throwAssertion("surname should have the value \"Moreno\"");
        }
        Address validCorrespondenceAddress = new Address();
        validCorrespondenceAddress.setAddress1("117 Old Shoreham Road");
        validCorrespondenceAddress.setAddress2("Hove");
        validCorrespondenceAddress.setAddress3("East Sussex");
        validCorrespondenceAddress.setAddress4("");
        validCorrespondenceAddress.setPostcode("BN3 7AQ");
        if (!applicant.getCorrespondenceAddress().equals(validCorrespondenceAddress)) {
            throwAssertion("correspondenceAddress should have the value: Line1, line2, Line3, Line4, Postcode");
        }
        Address validHomeAddress = new Address();
        validHomeAddress.setAddress1("87A Old Shoreham Road");
        validHomeAddress.setAddress2("Hove");
        validHomeAddress.setAddress3("East Sussex");
        validHomeAddress.setAddress4("aaa");
        validHomeAddress.setPostcode("BN3 7AQ");
        if (!applicant.getHomeAddress().equals(validHomeAddress)) {
            throwAssertion("homeAddress should have the value: Line1, line2, Line3, Line4, Postcode");
        }
        if (!applicant.getTpzAprId().equals(429L)) {
            throwAssertion("tpzAprId should have the value 429L");
        }
        if (!applicant.getTrfCtyIdBirth().equals(310L)) {
            throwAssertion("trfCtyIdBirth should have the value 310L");
        }
        if (!applicant.getTrfDscId().equals(45L)) {
            throwAssertion("trfDscId should have the value 45L");
        }
        if (applicant.getTrfEthId() != null) {
            throwAssertion("trfEthId should be null");
        }
        if (!applicant.getTrfMrsId().equals(1L)) {
            throwAssertion("trfMrsId should have the value 1L");
        }
        if (!applicant.getTrfNatId().equals(198L)) {
            throwAssertion("trfNatId should have the value 198L");
        }
        if (applicant.getTrfNatIdDual() != null) {
            throwAssertion("trfNatIdDual should be null");
        }
        if (applicant.getTrfNidId1() != null) {
            throwAssertion("trfNidId1 should be null");
        }
        if (applicant.getTrfNidId2() != null) {
            throwAssertion("trfNidId2 should be null");
        }
        if (!applicant.getTrfRscId().equals(27L)) {
            throwAssertion("trfRscId should have th value 27L");
        }
        if (!applicant.getTrfTtlId().equals(41L)) {
            throwAssertion("trfTtlId should have the value 41L");
        }
                
        return muleEvent;
    }

    // Convert a date of the format "2017-07-25T12:34:43.913Z" into Tuesday July 25th 
    private Date convertDate(String text) {
        String[] dateChunks = text.split("T");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return sdf.parse(dateChunks[0]);
        } catch (ParseException exception) {
            System.out.println("Error occured inside convertDate() within ValidUKPassModelAssertion.java when creating a Date object.");
            exception.printStackTrace();
        }
        return null;
    }
    
    private void throwAssertion(String message) {
        throw new AssertionError(message);
    }
    
}
