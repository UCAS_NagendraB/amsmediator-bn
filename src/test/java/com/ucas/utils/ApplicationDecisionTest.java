package com.ucas.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ApplicationDecisionTest {

	private Map<String, Object> application;
	private Map<String, Object> type;

	@Before
	public void setUp() {
		application = new HashMap<String, Object>();
		type = new HashMap<String, Object>();
	}

	@Test
	public void testDeriveCode_ReferredDecision() {

		type.put("Type", null);
		application.put("Decision", type);

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_REFERRED, ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_InterviewDecision() {

		type.put("Type", "InterviewRequired");
		application.put("Decision", type);

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_INTERVIEW, ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_ConditionalOfferDecision() {

		type.put("Type", "ConditionalOffer");
		application.put("Decision", type);

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_CONDITIONAL_OFFER,
				ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_UnconditionalOfferDecision_NonConfirmation() {

		type.put("Type", "UnconditionalOffer");
		application.put("Decision", type);
		application.put("State", "Proposed");

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER,
				ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_ConditionalOfferDecision_ConfirmationNoChange() {

		type.put("Type", "ConditionalOffer");
		application.put("Decision", type);
		application.put("StatusCode", "ConfirmedConditionalOffer");

		// Same course option Ids
		String courseUnderConsiderationOptionId = "123-456";
		String opportunityOptionId = "123-456";

		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseOptionId", courseUnderConsiderationOptionId);
		application.put("CourseUnderConsideration", courseUnderConsideration);

		Map<String, Object> app = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		opportunity.put("CourseOptionId", opportunityOptionId);
		app.put("Opportunity", opportunity);
		application.put("Application", app);

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_NO_CHANGE,
				ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_ConditionalOfferDecision_ConfirmationWithChange() {

		type.put("Type", "ConditionalOffer");
		application.put("Decision", type);
		application.put("StatusCode", "ConfirmedConditionalOffer");

		// Different course option Ids
		String courseUnderConsiderationOptionId = "123-456";
		String opportunityOptionId = "456-789";

		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseOptionId", courseUnderConsiderationOptionId);
		application.put("CourseUnderConsideration", courseUnderConsideration);

		Map<String, Object> app = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		opportunity.put("CourseOptionId", opportunityOptionId);
		app.put("Opportunity", opportunity);
		application.put("Application", app);

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE,
				ApplicationDecision.deriveCode(application));
	}
	
	@Test
	public void testDeriveCode_ConditionalOfferDecision_ConfirmationOfferDeclinedWithNoCourseChange() {
	
		type.put("Type", "ConditionalOffer");
		application.put("Decision", type);
		application.put("StatusCode", "DeclinedUnconditionalOffer");
		
		// Same course option Ids
		String courseUnderConsiderationOptionId = "123-456";
		String opportunityOptionId = "123-456";
	
		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseOptionId", courseUnderConsiderationOptionId);
		application.put("CourseUnderConsideration", courseUnderConsideration);

		Map<String, Object> app = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		opportunity.put("CourseOptionId", opportunityOptionId);
		app.put("Opportunity", opportunity);
		application.put("Application", app);
		
		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_NO_CHANGE,
				ApplicationDecision.deriveCode(application));
	}
	
	@Test
	public void testDeriveCode_ConditionalOfferDecision_ConfirmationOfferDeclinedWithCourseChange() {
	
		type.put("Type", "ConditionalOffer");
		application.put("Decision", type);
		application.put("StatusCode", "DeclinedUnconditionalOffer");
		
		// Different course option Ids
		String courseUnderConsiderationOptionId = "123-456";
		String opportunityOptionId = "456-789";
	
		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseOptionId", courseUnderConsiderationOptionId);
		application.put("CourseUnderConsideration", courseUnderConsideration);

		Map<String, Object> app = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		opportunity.put("CourseOptionId", opportunityOptionId);
		app.put("Opportunity", opportunity);
		application.put("Application", app);
		
		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE,
				ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_RejectionDecision_NonConfirmation() {

		type.put("Type", "Rejected");
		application.put("Decision", type);
		application.put("State", "Provisional");

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_REJECTED, ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_RejectionDecision_Confirmation() {

		type.put("Type", "Rejected");
		application.put("Decision", type);
		application.put("StatusCode", "RejectedAcceptedConditional");

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_REJECTED_CONF, ApplicationDecision.deriveCode(application));
	}

	@Test
	public void testDeriveCode_WithdrawnDecision() {

		type.put("Type", "Withdrawn");
		application.put("Decision", type);
		application.put("State", "Provisional");

		Assert.assertEquals(ApplicationDecision.LEGACY_CODE_WITHDRAWN, ApplicationDecision.deriveCode(application));
	}
}
