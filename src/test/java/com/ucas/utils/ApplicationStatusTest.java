package com.ucas.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ApplicationStatusTest {

	private Map<String, Object> applicationData;

	@Before
	public void setUp(){
		applicationData = new HashMap<String, Object>();
	}

	@Test
	public void testDeriveStatus_Draft(){

		applicationData.put("StatusCode", "Draft");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DRAFT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_Referred(){

		applicationData.put("StatusCode", "Referred");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_REFERRED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_Rejected(){

		applicationData.put("StatusCode", "Rejected");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_REJECTED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_DeclinedConditionalOffer(){

		applicationData.put("StatusCode", "DeclinedConditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DECLINED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_DeclinedUnconditionalOffer(){

		applicationData.put("StatusCode", "DeclinedUnconditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DECLINED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_RejectedAcceptedConditional(){

		applicationData.put("StatusCode", "RejectedAcceptedConditional");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_REJECTED_CONF, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ConsiderationWithdrawn(){

		Map<String, Object> decision = new HashMap<String, Object>();
		decision.put("DecisionReferenceCode", null);
		applicationData.put("Decision", decision);
		applicationData.put("StatusCode", "ConsiderationWithdrawn");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_WITHDRAWN, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ApplicationWithdrawn(){

		applicationData.put("StatusCode", "ApplicationWithdrawn");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_WITHDRAWN, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ApplicationWithdrawnByUcas_DeathOfApplicant(){

		Map<String, Object> ucasWithdrawalInformation = new HashMap<String, Object>();
		Map<String, Object> withdrawReason = new HashMap<String, Object>();
		withdrawReason.put("Id", "10006"); //10006 = Death of Applicant
		ucasWithdrawalInformation.put("WithdrawReason", withdrawReason);
		applicationData.put("UcasWithdrawalInformation", ucasWithdrawalInformation);
		applicationData.put("StatusCode", "ApplicationWithdrawnByUcas");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_CANCELLED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ApplicationWithdrawnByUcas_FraudulentApplication(){

		Map<String, Object> ucasWithdrawalInformation = new HashMap<String, Object>();
		Map<String, Object> withdrawReason = new HashMap<String, Object>();
		withdrawReason.put("Id", "10007"); //10007 = Fraudulent application
		ucasWithdrawalInformation.put("WithdrawReason", withdrawReason);
		applicationData.put("UcasWithdrawalInformation", ucasWithdrawalInformation);
		applicationData.put("StatusCode", "ApplicationWithdrawnByUcas");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_CANCELLED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ApplicationWithdrawnByUcas_DuplicateApplication(){

		Map<String, Object> ucasWithdrawalInformation = new HashMap<String, Object>();
		Map<String, Object> withdrawReason = new HashMap<String, Object>();
		withdrawReason.put("Id", "10008"); //10008 = Duplicate application
		ucasWithdrawalInformation.put("WithdrawReason", withdrawReason);
		applicationData.put("UcasWithdrawalInformation", ucasWithdrawalInformation);
		applicationData.put("StatusCode", "ApplicationWithdrawnByUcas");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_CANCELLED, ApplicationStatus.deriveStatus(applicationData));
	}

	@Test
	public void testDeriveStatus_Interview(){

		applicationData.put("StatusCode", "Interview");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_INTERVIEW, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ConditionalOffer(){

		applicationData.put("StatusCode", "ConditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_CONDITIONAL_OFFER, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_UnconditionalOffer(){

		applicationData.put("StatusCode", "UnconditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_AcceptedConditionalOffer(){

		applicationData.put("StatusCode", "AcceptedConditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_CONDITIONAL_ACCEPT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_Archived(){

		applicationData.put("StatusCode", "Archived");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_ARCHIVED, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_AcceptedUnconditionalOffer(){

		applicationData.put("StatusCode", "AcceptedUnconditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_ACCEPT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ConfirmedConditionalOffer_SameCourse(){

		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		Map<String, Object> application = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseId", "5c875d50-792d-41f0-99f9-d6c29dba5325");
		opportunity.put("CourseId", "5c875d50-792d-41f0-99f9-d6c29dba5325");
		application.put("Opportunity",opportunity);
		applicationData.put("CourseUnderConsideration", courseUnderConsideration);
		applicationData.put("Application", application);
		applicationData.put("StatusCode", "ConfirmedConditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_NO_CHANGE, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_ConfirmedConditionalOffer_ChangedCourse(){

		Map<String, Object> courseUnderConsideration = new HashMap<String, Object>();
		Map<String, Object> application = new HashMap<String, Object>();
		Map<String, Object> opportunity = new HashMap<String, Object>();
		courseUnderConsideration.put("CourseId", "5c875d50-792d-41f0-99f9-d6c29dba5325");
		opportunity.put("CourseId", "1c85tr5w-792d-21f0-9d8d-d6c29hy76tr4");
		application.put("Opportunity",opportunity);
		applicationData.put("CourseUnderConsideration", courseUnderConsideration);
		applicationData.put("Application", application);
		applicationData.put("StatusCode", "ConfirmedConditionalOffer");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_AwaitingPaymentConclusion(){

		applicationData.put("StatusCode", "AwaitingPaymentConclusion");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DRAFT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_AwaitingPaymentUnlock(){

		applicationData.put("StatusCode", "AwaitingPaymentUnlock");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DRAFT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_SubmissionStarted(){

		applicationData.put("StatusCode", "SubmissionStarted");

		Assert.assertEquals(ApplicationStatus.LEGACY_STATUS_DRAFT, ApplicationStatus.deriveStatus(applicationData));
	}
	
	@Test
	public void testDeriveStatus_UnknownStatus(){

		applicationData.put("StatusCode", "UnknownStatus");

		Assert.assertEquals("", ApplicationStatus.deriveStatus(applicationData));
	}
}
