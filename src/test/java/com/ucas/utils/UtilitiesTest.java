package com.ucas.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class UtilitiesTest {
	
	@Test
	public void testGetDateFromISODateTimeString() {

		String exampleISODateTimeString = "2016-05-12T14:38:02.9787592Z";
		
		Date asDate = Utilities.getDateFromISODateTimeString(exampleISODateTimeString);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String expectedOutput = "12-05-2016 14:38:02";

		Assert.assertEquals(expectedOutput, sdf.format(asDate));
	}
}
