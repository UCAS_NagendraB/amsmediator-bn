%dw 1.0
%output application/java
---
[{
	"trf_nat_id": 29,
	"code": "774",
	"description": "Vietnamese",
	"ams_code": "774",
	"ams_description": "Vietnamese"
}, {
	"trf_nat_id": 30,
	"code": "780",
	"description": "Serbian",
	"ams_code": "780",
	"ams_description": "Serbian"
}, {
	"trf_nat_id": 31,
	"code": "781",
	"description": "Zambian",
	"ams_code": "781",
	"ams_description": "Zambian"
}]