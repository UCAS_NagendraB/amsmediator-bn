%dw 1.0
%output application/java
---
[{
	"trf_eth_id": 10,
	"code": "11",
	"description": "White",
	"hesa_code": "10",
	"ams_code": "10001",
	"ams_description": "White"
}, {
	"trf_eth_id": 19,
	"code": "21",
	"description": "Black - Caribbean",
	"hesa_code": "21",
	"ams_code": "10003",
	"ams_description": "Black - Caribbean"
}, {
	"trf_eth_id": 20,
	"code": "22",
	"description": "Black - African",
	"hesa_code": "22",
	"ams_code": "10004",
	"ams_description": "Black - African"
}]