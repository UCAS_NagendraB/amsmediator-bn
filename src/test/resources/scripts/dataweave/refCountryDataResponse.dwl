%dw 1.0
%output application/java
---
[{
	"trf_cty_id": 583,
	"code": "653",
	"description": "France",
	"ams_code": "653",
	"ams_description": "France"
}, {
	"trf_cty_id": 686,
	"code": "751",
	"description": "Spain",
	"ams_code": "751",
	"ams_description": "Spain"
}, {
	"trf_cty_id": 589,
	"code": "589",
	"description": "Germany",
	"ams_code": "589",
	"ams_description": "Germany"
}, {
	"trf_cty_id": 725,
	"code": "000",
	"description": "United Kingdom",
	"ams_code": "000",
	"ams_description": "United Kingdom"
}]