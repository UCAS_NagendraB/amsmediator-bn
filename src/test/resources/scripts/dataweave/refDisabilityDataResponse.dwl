%dw 1.0
%output application/java
---
[{
	"trf_dsc_id": 51,
	"code": "G",
	"description": "Learning difficulty",
	"ams_code": "G",
	"ams_description": "Learning difficulty"
}, {
	"trf_dsc_id": 52,
	"code": "H",
	"description": "Wheelchair/mobility",
	"ams_code": "H",
	"ams_description": "Wheelchair/mobility"
}, {
	"trf_dsc_id": 53,
	"code": "I",
	"description": "Other disability",
	"ams_code": "I",
	"ams_description": "Other disability"
}]