%dw 1.0
%output application/java
---
[{
	"trf_apr_id": 1,
	"code": "919",
	"description": "Hertfordshire",
	"ams_code": "919",
	"ams_description": "Hertfordshire"
}, {
	"trf_apr_id": 2,
	"code": "921",
	"description": "United Kingdom",
	"ams_code": "921",
	"ams_description": "United Kingdom"
}, {
	"trf_apr_id": 3,
	"code": "922",
	"description": "Kent",
	"ams_code": "922",
	"ams_description": "Kent"
}]