%dw 1.0
%output application/java
---
[{
	"trf_ttl_id": 25,
	"code": "00071",
	"description": "Sir",
	"ams_code": "1045",
	"ams_description": "Sir"
}, {
	"trf_ttl_id": 26,
	"code": "00077",
	"description": "Sr",
	"ams_code": "1046",
	"ams_description": "Senor"
}, {
	"trf_ttl_id": 27,
	"code": "00080",
	"description": "Visc",
	"ams_code": "1049",
	"ams_description": "Viscount"
}]