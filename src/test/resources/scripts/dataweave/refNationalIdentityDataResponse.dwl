%dw 1.0
%output application/java
---
[{
	"trf_nid_id": 1,
	"code": "B",
	"description": "British",
	"ams_code": "B",
	"ams_description": "British"
}, {
	"trf_nid_id": 2,
	"code": "E",
	"description": "English",
	"ams_code": "E",
	"ams_description": "English"
}, {
	"trf_nid_id": 3,
	"code": "I",
	"description": "Irish",
	"ams_code": "I",
	"ams_description": "Irish"
}]