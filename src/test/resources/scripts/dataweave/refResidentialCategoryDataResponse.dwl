%dw 1.0
%output application/java
---
[{
	"trf_rsc_id": 21,
	"code": "1",
	"description": "UK Citizen or EU National",
	"ams_code": "1",
	"ams_description": "UK Citizen or EU National"
}, {
	"trf_rsc_id": 22,
	"code": "2",
	"description": "EEA or Swiss national",
	"ams_code": "2",
	"ams_description": "EEA or Swiss national"
}, {
	"trf_rsc_id": 23,
	"code": "3",
	"description": "Child of a Turkish Worker",
	"ams_code": "3",
	"ams_description": "Child of a Turkish Worker"
}]