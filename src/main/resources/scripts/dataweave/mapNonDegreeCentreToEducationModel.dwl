%dw 1.0
%output application/java
%var DegreeQualTypeId = 4
%var nonDegreeCentreIds = (flowVars.ApplicationData.LearnerProfile.Education.Qualifications filter ($.QualificationType != DegreeQualTypeId)).PlaceOfStudyId distinctBy $.PlaceOfStudyId default []
%var degreeCentreIds = (flowVars.ApplicationData.LearnerProfile.Education.Qualifications filter ($.QualificationType == DegreeQualTypeId)).PlaceOfStudyId distinctBy $.PlaceOfStudyId default []
%var degreeOnlyCentreIds = degreeCentreIds -- nonDegreeCentreIds
---
flowVars.ApplicationData.LearnerProfile.Education.PlacesOfStudy default [] filter not (degreeOnlyCentreIds contains $.Id) map ((centre) -> {
	tpzAptId : flowVars.applicant.tpzAptId,
	noFormalQualificationsFlag : "N" 
		when centre.AnyQualificationsAtThisVenue == true 
		otherwise "Y",
	resultsAwaitedFlag : null,
	finishDate : centre.EndDate as :date,
	startDate : centre.StartDate as :date,
//	lastUpdated : DONE VIA TRIGGER,
	universityOrder : null, 
	nupInsId : null, 
	trfCtyId : lookup("dataLookupFlow",{
			"returnColumn": "trf_cty_id",
			"keyColumn": "ams_code",
			"dataflowname": "getCountryData", 
			"lookupValue": centre.CountryOfStudy.Id}) default null,
	tpzSchId : null,
	tpzAddId : null,
	ukpUetId : "2", // Education establishment (Non-HEI)
	ukpUedId : null, //primary key
	dateTakenMonth : null,
	dateTakenYear : null,
	candidateNumber : null,
	additionalInformation : null,
	department : null,
	establishmentName : centre.Name,
	details : null,
	title : null,
	attendance : "FT" when centre.StudyType == "0"
		otherwise "PT"  when centre.StudyType == "1"
		otherwise null,
	grade : null,
	subject : null,
	awardSubject : null,
	dissertationResearch : null,
	awardLevel : null,
	language : null,
	awardingBody : null
} as :object {
	class: "com.ucas.ukpass.model.Education"
}
)