%dw 1.0
%output application/java
%var DegreeQualTypeId = 4
%var EducationCentres = flowVars.ApplicationData.LearnerProfile.Education.PlacesOfStudy
%function getCentreField(centreId, fieldName) ((EducationCentres filter $.Id == centreId) reduce $).'$fieldName' default null
%function getQualField(qualification, fieldName) ((qualification.Fields filter $.Key == fieldName).Value[0])
%var emptyStringToNull = (inStr) -> null when trim inStr == '' otherwise inStr
---
//Filter quals on QualificationType == 4 (Degree)
flowVars.ApplicationData.LearnerProfile.Education.Qualifications default [] filter ($.QualificationType == DegreeQualTypeId) map ((qualification) -> {
	tpzAptId : flowVars.applicant.tpzAptId,
	noFormalQualificationsFlag : "N", 
	resultsAwaitedFlag : "Y" 
		when emptyStringToNull(getQualField(qualification, "Result")) == null 
		otherwise "N", 
	finishDate : getCentreField(qualification.PlaceOfStudyId, "EndDate") as :date,
	startDate : getCentreField(qualification.PlaceOfStudyId, "StartDate") as :date,
//	lastUpdated : DONE VIA TRIGGER,
	universityOrder : null,
	nupInsId : null,
	trfCtyId : lookup("dataLookupFlow",{
			"returnColumn": "trf_cty_id",
			"keyColumn": "ams_code",
			"dataflowname": "getCountryData", 
			"lookupValue": getCentreField(qualification.PlaceOfStudyId, "CountryOfStudy").Id}) default null,
	tpzSchId : null,
	tpzAddId : null,
	ukpUetId : "4", // Higher Education Institution Details (used for degree location & qual)
	ukpUedId : null, //primary key
	dateTakenMonth : getQualField(qualification, "CertificationDate") as :date as :string{format: "MM"} default null,
	dateTakenYear : getQualField(qualification, "CertificationDate") as :date as :string{format: "yyyy"} default null,
	candidateNumber : null,
	additionalInformation : null,
	department : null,
	establishmentName : getCentreField(qualification.PlaceOfStudyId, "Name"),
	details : null,
	title : substring(emptyStringToNull(getQualField(qualification, "CourseTitle")),0,100),
	attendance : "FT" 
		when getCentreField(qualification.PlaceOfStudyId, "StudyType") == "0"
		otherwise "PT" when getCentreField(qualification.PlaceOfStudyId, "StudyType") == "1"
		otherwise null,
	grade : emptyStringToNull(getQualField(qualification, "Result")),
	subject : emptyStringToNull(getQualField(qualification, "Subject")),
	awardSubject : null,
	dissertationResearch : null,
	awardLevel : substring(getQualField(qualification, "DegreeType"),0,30),
	language : null,
	awardingBody : null
} as :object {
	class: "com.ucas.ukpass.model.Education"
}
)