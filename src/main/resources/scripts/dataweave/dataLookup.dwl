%dw 1.0
%output application/java
%var returnColumn = flowVars.lookupParams.returnColumn
%var keyColumn = flowVars.lookupParams.keyColumn
---
{
    lookedupcode: (payload map {
        "return_value" : $[returnColumn],
        "key_value" : $[keyColumn]
        } filter $.key_value == flowVars.lookupParams.lookupValue)[0].return_value
}