%dw 1.0
%output application/java
---
{
	studymode: (payload map {
        "STUDY_MODE_CODE" : $.STUDY_MODE_CODE,
        "STUDY_MODE_TYPE" : $.STUDY_MODE_TYPE
        } filter $.STUDY_MODE_TYPE == (lower flowVars.ApplicationData.CourseUnderConsideration.StudyMode))[0].STUDY_MODE_CODE
}