%dw 1.0
%output application/java
%var TAUGHTOFFSET = 10000
%var RESEARCHOFFSET = 20000
%var qualification = flowVars.ApplicationData.CourseUnderConsideration.Qualification
%var deliveryMethod = flowVars.ApplicationData.CourseUnderConsideration.StudyType
---
((flowVars.outcomeQualifications filter $ == qualification pluck $$) reduce $$) as :string as :number
+ 
(TAUGHTOFFSET when deliveryMethod == "Taught" 
otherwise RESEARCHOFFSET)