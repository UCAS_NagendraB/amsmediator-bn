%dw 1.0
%output application/java
%var emptyStringToNull = (inStr) -> null when trim inStr == '' otherwise inStr
---
flowVars.ApplicationData.LearnerProfile.EnglishLanguage.ProficiencyQualifications default [] map ((proficiencyQualification) -> {
	tpzAptId : flowVars.applicant.tpzAptId,
	noFormalQualificationsFlag : "N", 
	resultsAwaitedFlag : null,
	finishDate : null,
	startDate : null,
//	lastUpdated : DONE VIA TRIGGER,
	universityOrder : null,
	nupInsId : null,
	trfCtyId : null,
	tpzSchId : null,
	tpzAddId : null,
	ukpUetId : "7", // English Proficiency Test
	ukpUedId : null, //primary key
	dateTakenMonth : proficiencyQualification.QualificationDate.TestDate as :date{format: "dd/MM/yyyy"} as :string{format: "MM"} default null,
	dateTakenYear : proficiencyQualification.QualificationDate.TestDate as :date{format: "dd/MM/yyyy"} as :string{format: "yyyy"} default null,
	candidateNumber : substring(proficiencyQualification.CandidateNumber,0,10),
	additionalInformation : null,
	department : null,
	establishmentName : proficiencyQualification.TestCentre,
	details : substring(emptyStringToNull(proficiencyQualification.OtherGradeInformation),0,100),
	title : substring(proficiencyQualification.OtherQualification,0,100)
		when proficiencyQualification.OtherQualification != null
		otherwise substring(proficiencyQualification.TestType.Caption,0,100),
	attendance : null,
	grade : substring(proficiencyQualification.TestGrade,0,20),
	subject : null,
	awardSubject : null,
	dissertationResearch : null,
	awardLevel : null,
	language : null,
	awardingBody : null
} as :object {
	class: "com.ucas.ukpass.model.Education"
}
)