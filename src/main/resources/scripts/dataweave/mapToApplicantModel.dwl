%dw 1.0
%output application/java
---
{
    btecRegNo : null,  
    crbCheckFlag : null default null,
    criminalConvictionFlag : boolNullYNChar(flowVars.ApplicationData.CriminalConvictions.HaveEnhancedUnspentConviction),
    dateOfBirth : convertDate(flowVars.ApplicationData.LearnerProfile.PersonalDetails.DateOfBirth),
    dateUKEntry : convertDate(flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.DateOfEntryToUK),
    emailAddress : flowVars.ApplicationData.LearnerProfile.ContactDetails.EmailAddress,
    emailAddress2 : null,
    emailAddressVerifiedFlag : "Y",
    emailOptInFlag : null,
    emergencyContactName : null,
    emergencyContactTelephone : null,
    forenames : flowVars.ApplicationData.LearnerProfile.PersonalDetails.FirstName,
    homeTelephone : flowVars.ApplicationData.LearnerProfile.ContactDetails.OtherPhoneNumber,
    ielts : null,
    indefiniteLeaveUk : null,
    //initials : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
    language : null,
    largeFontRequiredFlag : "N",
    mobileTelephone : flowVars.ApplicationData.LearnerProfile.ContactDetails.MobilePhoneNumber,
    nagtyFlag : null,
    nagtyType : null,
    nationalInsuranceNumber : null,
    noOfDependents : null,
    passportNumber : flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Passport.Number,
    passportIssueDate : convertDate(flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Passport.ValidFromDate),
    passportExpiryDate : convertDate(flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Passport.ExpiryDate),
    passportPlaceOfIssue : flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Passport.PlaceOfIssue.Caption,
    permanentHomeUk : "Y" when flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.IsHomeAddressInUK == true otherwise "N",
    personalId: flowVars.ApplicationData.LearnerProfile.PID,
    preferredFirstName : flowVars.ApplicationData.LearnerProfile.PersonalDetails.PreferredName,
    previousSurname : flowVars.ApplicationData.LearnerProfile.PersonalDetails.PreviousNames,
    scottishCandidateNo : null,
    sex : flowVars.ApplicationData.LearnerProfile.PersonalDetails.Gender.Caption[0..0],
    specialNeeds : flowVars.ApplicationData.LearnerProfile.Support.DisabilityInformation,
    studentVisaRequired : "Y" when flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Visa.IsStudyVisaRequired == true otherwise "N",
    surname: flowVars.ApplicationData.LearnerProfile.PersonalDetails.FamilyName,
    correspondenceAddress: using (CorrespondenceAddress = flowVars.ApplicationData.LearnerProfile.ContactDetails.PostalAddress[0]) {
             address1: CorrespondenceAddress.Line1 default "-",
             address2: CorrespondenceAddress.Line2,
             address3: CorrespondenceAddress.Line3,
             address4: CorrespondenceAddress.Line4,
             postcode: CorrespondenceAddress.Postcode,
             primaryAddressFlag: "Y",
             trfCtyId : lookup("dataLookupFlow",{
                 "returnColumn": "trf_cty_id",
                 "keyColumn": "ams_code",
                 "dataflowname": "getCountryData", 
                 "lookupValue": CorrespondenceAddress.Country.Id}) default null when CorrespondenceAddress.Country != null otherwise null
             //version: "REMOVE FROM OBJECT - DONE BY DB TRIGGER"
       } as :object {
             class: "com.ucas.ukpass.model.Address"
       },
    homeAddress: using (HomeAddress = flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.PermanentHomeAddress) {
             address1: HomeAddress.Line1,
             address2: HomeAddress.Line2,
             address3: HomeAddress.Line3,
             address4: HomeAddress.Line4,
             postcode: HomeAddress.Postcode,
             primaryAddressFlag: "N",
             trfCtyId : lookup("dataLookupFlow",{
                 "returnColumn": "trf_cty_id",
                 "keyColumn": "ams_code",
                 "dataflowname": "getCountryData", 
                 "lookupValue": HomeAddress.Country.Id}) default null when HomeAddress.Country != null otherwise null
             //version: "REMOVE FROM OBJECT - DONE BY DB TRIGGER"
       } as :object {
             class: "com.ucas.ukpass.model.Address"
       },
    toefl : null,       
    tpzAptId: null,
    tpzAprId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_apr_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getAreaOfPermanentResidenceData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.AreaOfPermanentResidence.Id}) default 429,
    trfCtyIdBirth: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_cty_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getCountryData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.CountryOfBirth.Id}) default 310,
    trfDscId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_dsc_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getDisabilityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.Support.DisabilityType.Id}) default 45,
    trfEthId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_eth_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getEthnicityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.Diversity.Ethnicity.Id}),
    trfMrsId: 1,
    trfNatId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_nat_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getNationalityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Nationality[0].Id}) default 198,
    trfNatIdDual: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_nat_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getNationalityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.Nationality[1].Id}) default null,
    trfNidId1: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_nid_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getNationalIdentityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.Diversity.NationalIdentity.Id}),
    trfNidId2: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_nid_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getNationalIdentityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.Diversity.NationalIdentityOther.Id}),
    trfProId: null,
    trfRscId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_rsc_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getResidentialCategoryData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.ResidencyAndNationality.ResidentialStatus.Id}) default 27,
    trfTtlId: lookup("dataLookupFlow",{
       	        "returnColumn": "trf_ttl_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getTitleData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.PersonalDetails.Title.Id}) default 41,
    unicodeMsgShown : null
    //version: "REMOVE FROM OBJECT - DONE BY DB TRIGGER"
} as :object {
    class: "com.ucas.ukpass.model.Applicant"
}