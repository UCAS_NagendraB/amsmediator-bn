%function split(text,char) text splitBy char
%function getRejectReasonDescriptionFromCode(rejectionCode) lookup("dataLookupFlow",{"returnColumn": "description", "keyColumn": "ams_code", dataflowname: "getRejectionReasonData", "lookupValue": rejectionCode})
%function getWithdrawReasonDescriptionFromCode(withdrawalCode) lookup("dataLookupFlow",{"returnColumn": "description", "keyColumn": "ams_code", "dataflowname": "getProviderWithdrawalReasonData", "lookupValue": withdrawalCode})
%dw 1.0
%output application/java
---
{
  //ukpUapId : "Set by later setUkpUapIdFlow",
  //tpzAptId : "Set by later setTpzAptIdFlow",
  ukpUicId : flowVars.LegacyCourseId[0].UKP_UIC_ID,
  agentUsed : 'Y' when flowVars.ApplicationData.Agent.IsAgentInUse == true otherwise 'N',
  agentCode : flowVars.ApplicationData.Agent.AgencyCode,
  agentName : flowVars.ApplicationData.Agent.AgencyName,
  // agentNameU : no need to set this, as it is the Unicode version of agentName, and is set using code in the agentName property in the javaObject     
  agentEmailAddress : flowVars.ApplicationData.Agent.EmailAddress,
  (agentAddress: using (AgentAddress = flowVars.ApplicationData.Agent.Address) {
             address1: AgentAddress.Line1,
             address2: AgentAddress.Line2,
             address3: AgentAddress.Line3,
             address4: AgentAddress.Line4,
             primaryAddressFlag: "Y",
             trfCtyId : lookup("dataLookupFlow",{
                 "returnColumn": "trf_cty_id",
                 "keyColumn": "ams_code",
                 "dataflowname": "getCountryData", 
                 "lookupValue": AgentAddress.Country.Id}) default null when AgentAddress.Country != null otherwise null,
             version: null
       } as :object {
             class: "com.ucas.ukpass.model.Address"
       }) when flowVars.ApplicationData.Agent.IsAgentInUse == true and flowVars.ApplicationData.Agent.Address.Line1?,
  ukpUcaId : null,//not required
  ukpUqlId : flowVars.LegacyCourseId[0].UKP_UQL_ID,
  trfAwrId : null,//not required
  trfDscId : lookup("dataLookupFlow",{
       	        "returnColumn": "trf_dsc_id",
	 	        "keyColumn": "ams_code",
	 	        "dataflowname": "getDisabilityData", 
	 	        "lookupValue": flowVars.ApplicationData.LearnerProfile.Support.DisabilityType.Id}),
  //ukpUicIdPrevious : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  ukpUicIdEnrolled : null,//null initially
  tpzAdsId : 21,//21=UKPASS admissions scheme
  applicationCode : ('P' ++ flowVars.ApplicationData.SubmittedApplicationNumber as :string {format:"000"}) 
  					when flowVars.ApplicationSchemeCode == "App Scheme Code not found"
  					otherwise flowVars.ApplicationSchemeCode,
  criminalConvictionFlag : boolNullYNChar(flowVars.ApplicationData.CriminalConvictions.HaveEnhancedUnspentConviction), 
  //dateApnLastModified : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  dateArchived : null as :date default null,//not required
  //dateToIns : "DONE VIA TRIGGER",
  //dateToTopaz : "DONE VIA TRIGGER",
  dateSubmitted : flowVars.messageTimestamp,
  //decision : "POPULATED LATER IN PROCESS",
  //decisionOriginal : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  disSpecialNeedsText : flowVars.ApplicationData.LearnerProfile.Support.DisabilityInformation,
  enrolledDate : null,//not required
  fundingEmployerFlag : "Y" when (flowVars.ApplicationData.Funding.Method.Id == "10001") 
                            otherwise "N",
  fundingOther : "Y" when (flowVars.ApplicationData.Funding.Method.Id == "10002" 
  	                       or flowVars.ApplicationData.Funding.Method.Id == "10003" 
  	                       or flowVars.ApplicationData.Funding.Method.Id == "10005")
  	                 otherwise "N",
  fundingScholarship : "Y" when (flowVars.ApplicationData.Funding.Method.Id == "10004") 
                           otherwise "N",
  fundingSelf : "Y" when (flowVars.ApplicationData.Funding.Method.Id == "10000") 
                    otherwise "N",
  fundingText : "",//not required
  offerText : flowVars.ApplicationData.Offer.Conditions.ConditionInformation joinBy " "
                 when flowVars.ApplicationData.Offer.Conditions.ConditionInformation? otherwise 
                 flowVars.ApplicationData.Decision.Description default "" when flowVars.ApplicationData.Decision.Type == 'Rejected' or flowVars.ApplicationData.Decision.Type == 'Withdrawn'
                 otherwise "",
  origination : "AMS",
  response : null when flowVars.ApplicationData.Offer.LearnerResponse.Type == null
              otherwise 'ACC' when flowVars.ApplicationData.Offer.LearnerResponse.Type contains "Accepted"
              otherwise 'DEC' when flowVars.ApplicationData.Offer.LearnerResponse.Type contains "Declined" 
              otherwise null,
  //responseOriginal : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  startMonth : convertMonthToInt(flowVars.ApplicationData.CourseUnderConsideration.StartDate) default null,
  startYear : convertYearToInt(flowVars.ApplicationData.CourseUnderConsideration.StartDate) default null,
  //status : "POPULATED LATER IN PROCESS",
  //statusOld : "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  //versionNumber :  "REMOVE FROM OBJECT - DONE BY DB TRIGGER",
  //lastUpdated : "DONE BY DB TRIGGER",  
  removedPersonalId : null,//Not used in XML Link
  removedApplicationCode : null,//Not used in XML Link
  decReason : getRejectReasonDescriptionFromCode(flowVars.ApplicationData.Decision.DecisionReferenceCode.Id) 
  	when flowVars.ApplicationData.Decision.Type == 'Rejected' and flowVars.ApplicationData.Decision.DecisionReferenceCode.Id != null
  	otherwise getWithdrawReasonDescriptionFromCode(flowVars.ApplicationData.Decision.DecisionReferenceCode.Id) 
  		when flowVars.ApplicationData.Decision.Type == 'Withdrawn' and flowVars.ApplicationData.Decision.DecisionReferenceCode.Id != null 
  		otherwise "",
  personalStatement : flowVars.ApplicationData.PersonalStatement.Statement when not flowVars.ApplicationData.PersonalStatement.Statement == null otherwise "",
  underConsideration : null,//not required
  unicodeApprovalReq : "NNNNNN",
  unicodeApproval : "NNNNNN",
  unicodeMsgShown : 'N'	
} as :object {
	class: "com.ucas.ukpass.model.Application"
}