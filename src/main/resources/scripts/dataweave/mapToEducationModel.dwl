%dw 1.0
%output application/java
%var EducationCentres = flowVars.ApplicationData.LearnerProfile.Education.PlacesOfStudy
%function getCentreName(centreId)  (EducationCentres filter $.Id == centreId).Name  reduce $ default ""
%function getCentreStartDate(centreId) (EducationCentres filter $.Id == centreId).StartDate reduce $ as :date default null
%function getCentreEndDate(centreId) (EducationCentres filter $.Id == centreId).EndDate reduce $ as :date  default null
%function getField(qualification, fieldName) ((qualification.Fields filter $.Key == fieldName).Value[0])
%function getTitle(qualification) (getField(qualification, "CourseTitle") when qualification.QualificationTypeCaption == "Degree"
    							otherwise getField(qualification, "QualificationName"))
%function getAwardLevel(qualification) ((getField(qualification, "DegreeType") ++ " " ++ qualification.QualificationTypeCaption) when qualification.QualificationTypeCaption == "Degree"
    									otherwise qualification.QualificationTypeCaption)
%var emptyStringToNull = (inStr) -> null when trim inStr == '' otherwise inStr
---
//defaulting all quals to have aukpUeqId of 3 for now
//
flowVars.ApplicationData.LearnerProfile.Education.Qualifications map ((qualification) -> {
	tpzAptId : flowVars.applicant.tpzAptId,
    noFormalQualificationsFlag : "Y" when flowVars.ApplicationData.LearnerProfile.Education.HasNoQualifications == true otherwise "N", 
	resultsAwaitedFlag : "Y" when emptyStringToNull(getField(qualification, "Result")) == null otherwise "N", 
	finishDate : getCentreEndDate(qualification.PlaceOfStudyId),
	startDate : getCentreStartDate(qualification.PlaceOfStudyId),
//	lastUpdated : DONE VIA TRIGGER,
	universityOrder : null,
	nupInsId : null, //could we get this from PlaceOfStudy
	trfCtyId :	(lookup("getCountryData","") map 
					{
						"trfCtyId" : $.trf_cty_id,
						"desc" : $.ams_description
					} filter $.desc == (EducationCentres filter $.Id ==  qualification.PlaceOfStudyId).CountryOfStudy.Id)[0].trfCtyId,
	tpzSchId : null, //no idea
	tpzAddId : null, //never set in legacy
	ukpUetId : "3", // defaulting to 3
	ukpUedId : null, //primary key
	dateTakenMonth : ((qualification.Fields filter $.Key == "CertificationDate").Value reduce $) as :date{format: "dd MMM yyyy"} as :string{format: "MM"} default null,
	dateTakenYear : ((qualification.Fields filter $.Key == "CertificationDate").Value reduce $) as :date{format: "dd MMM yyyy"} as :string{format: "YYYY"} default null,
	candidateNumber : "",
	additionalInformation : "",
	department :  "",
	establishmentName : getCentreName(qualification.PlaceOfStudyId),
	details : "",
	title : getTitle(qualification),
	attendance : "",
	grade : emptyStringToNull(getField(qualification, "Result")),
	subject : emptyStringToNull(getField(qualification, "Subject")),
	awardSubject : "",
	dissertationResearch : "",
	awardLevel : getAwardLevel(qualification),
	language : "",
	awardingBody : emptyStringToNull(getField(qualification, "AwardingBodyName"))
} as :object {
	class: "com.ucas.ukpass.model.Education"
}
)
 
 