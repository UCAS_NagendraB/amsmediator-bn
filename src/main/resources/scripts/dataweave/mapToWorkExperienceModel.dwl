%dw 1.0
%output application/java
---
flowVars.ApplicationData.LearnerProfile.WorkExperience map ((WorkExperience) -> {
	ukpUweId : null,//pk set later
	tpzAptId : null,//set later,
	experienceAddress : using (ExperienceAddress = WorkExperience.OrganisationAddress) {
		address1: ExperienceAddress.Line1,
		address2: ExperienceAddress.Line2,
		address3: ExperienceAddress.Line3,
		address4: ExperienceAddress.Line4,
		postcode: ExperienceAddress.Postcode,
		trfCtyId : lookup("dataLookupFlow",{
			"returnColumn": "trf_cty_id",
			"keyColumn": "ams_code",
			"dataflowname": "getCountryData", 
			"lookupValue": ExperienceAddress.Country.Id}) default null when ExperienceAddress.Country != null otherwise null,
		version: null//DONE VIA TRIGGER
	} as :object {
		class: "com.ucas.ukpass.model.Address"
	},
	trfCtyId : (lookup("getCountryData","") map 
		{
			"trfCtyId" : $.trf_cty_id,
			"desc" : $.ams_description
		} filter $.desc == WorkExperience.OrganisationAddress.Country)[0].trfCtyId,
	name : WorkExperience.OrganisationName,
	telephone : WorkExperience.ContactNumber,
	jobDescription : WorkExperience.SummaryOfResponsibilities,
	startYear: ((WorkExperience.StartDate as :date) as :string {format: "yyyy"}) as :number when 
		WorkExperience.StartDate != null otherwise null,
	startMonth: ((WorkExperience.StartDate as :date) as :string {format: "MM"}) as :number when 
		WorkExperience.StartDate != null otherwise null,
	endYear: ((WorkExperience.EndDate as :date) as :string {format: "yyyy"}) as :number when 
		WorkExperience.EndDate != null otherwise null,
	endMonth: ((WorkExperience.EndDate as :date) as :string {format: "MM"}) as :number when 
		WorkExperience.EndDate != null otherwise null,
	workType: 'F' when WorkExperience.IsFullTime == true otherwise 'P',
	voluntaryFlag: 'N',//Not collected
	noWorkExperienceFlag: 'N',
	finishDateUnknown : 'Y' when WorkExperience.EndDate == null otherwise 'N',
	lastUpdated : null //DONE VIA TRIGGER
} as :object {
	class: "com.ucas.ukpass.model.WorkExperience"
}
) when (sizeOf flowVars.ApplicationData.LearnerProfile.WorkExperience) > 0 
otherwise [{
	ukpUweId : null,//pk set later
	tpzAptId : null,//set later,
	noWorkExperienceFlag: 'Y'
} as :object {
	class: "com.ucas.ukpass.model.WorkExperience"
}]
