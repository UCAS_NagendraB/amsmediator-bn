%dw 1.0
%input Application application/java
%output application/java
%var answersAddress = { 
			address1: null, address2: null, address3: null, address4: null,
			postcode: null, version: null
	} as :object {
		class: "com.ucas.ukpass.model.Address"
	}
---
flatten (flowVars.ApplicationData.ProviderQuestionDefinition.ProviderQuestions default [] map (providerQuestion, position) -> 
	(providerQuestion.UploadedDocuments filter $.StatusCode == "Available" default [] map (uploadedDocument) -> {
		lastUpdated: convertDate(uploadedDocument.LastUpdated),
		attachmentUploadDate: convertDate(uploadedDocument.Created),
		ukpUapId : null, //"Set by later setUkpUapIdFlow",
		answersAddress: answersAddress,
		position: position+1,
		questionText: providerQuestion.QuestionText,
		answer: providerQuestion.AnswerText,
		filename: uploadedDocument.ExternalId,
		attachmentAllowedFlag: "Y" when providerQuestion.RuleConclusions.MaximumProviderQuestionUploadsAllowed.NumberUploads > 0
			otherwise "N",
		attachmentExtension: (uploadedDocument.FileName splitBy ".")[-1] //Isolate trailing file extension
	} as :object {
		class: "com.ucas.ukpass.model.QuestionsAnswers"
	} when (sizeOf providerQuestion.UploadedDocuments) > 0
		otherwise {
		ukpUapId : null, //"Set by later setUkpUapIdFlow",
		answersAddress: answersAddress,
		position: position+1,
		questionText: providerQuestion.QuestionText replace "–" with "-",
		answer: providerQuestion.AnswerText,
		filename: null,
		attachmentAllowedFlag: "Y" when providerQuestion.RuleConclusions.MaximumProviderQuestionUploadsAllowed.NumberUploads > 0
			otherwise "N"
	} as :object {
		class: "com.ucas.ukpass.model.QuestionsAnswers"
	}) 
	) distinctBy ($.questionText ++ $.filename)