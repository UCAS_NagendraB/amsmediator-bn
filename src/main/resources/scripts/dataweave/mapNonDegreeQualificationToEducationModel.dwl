%dw 1.0
%output application/java
%var DegreeQualTypeId = 4
%function getQualField(qualification, fieldName) ((qualification.Fields filter $.Key == fieldName).Value reduce $ default null)
%var emptyStringToNull = (inStr) -> null when trim inStr == '' otherwise inStr
---
//Filter quals on QualificationType != 4 (Non-Degrees)
flowVars.ApplicationData.LearnerProfile.Education.Qualifications default [] filter ($.QualificationType != DegreeQualTypeId) map ((qualification) -> {
	tpzAptId : flowVars.applicant.tpzAptId,
	noFormalQualificationsFlag : "Y" 
		when flowVars.ApplicationData.LearnerProfile.Education.HasNoQualifications == true 
		otherwise "N", 
	resultsAwaitedFlag : "Y" 
		when emptyStringToNull(getQualField(qualification, "Result")) == null 
		otherwise "N", 
	finishDate : null,
	startDate : null,
//	lastUpdated : DONE VIA TRIGGER,
	universityOrder : null,
	nupInsId : null,
	trfCtyId : null,
	tpzSchId : null,
	tpzAddId : null,
	ukpUetId : "3", // Non-Degree qual
	ukpUedId : null, //primary key
	dateTakenMonth : getQualField(qualification, "CertificationDate") as :date as :string{format: "MM"} default null,
	dateTakenYear : getQualField(qualification, "CertificationDate") as :date as :string{format: "yyyy"} default null,
	candidateNumber : null,
	additionalInformation : null,
	department : null,
	establishmentName : null,
	details : null,
	title : substring(getQualField(qualification, "QualificationName"),0,100)
		when getQualField(qualification, "QualificationName") != null
		otherwise substring(qualification.QualificationTypeCaption,0,100),
	attendance : null,
	grade : emptyStringToNull(getQualField(qualification, "Result")),
	subject : emptyStringToNull(getQualField(qualification, "Subject")),
	awardSubject : null,
	dissertationResearch : null,
	awardLevel : substring(emptyStringToNull(getQualField(qualification, "Level")),0,30),
	language : null,
	awardingBody : substring(emptyStringToNull(getQualField(qualification, "AwardingBodyName")),0,50)
} as :object {
	class: "com.ucas.ukpass.model.Education"
}
)