%dw 1.0
%output application/java
---
{	
	correspondenceAddress: {
		tpzAddId:  payload[0].TPZ_ADD_ID_CORRESPONDENCE
	} as :object {
		class: "com.ucas.ukpass.model.Address"
	} when payload[0].TPZ_ADD_ID_CORRESPONDENCE != null otherwise null,
	homeAddress: {
		tpzAddId:  payload[0].TPZ_ADD_ID_HOME
	} as :object {
		class: "com.ucas.ukpass.model.Address"
	} when payload[0].TPZ_ADD_ID_HOME != null otherwise null
} as :object {
	class: "com.ucas.ukpass.model.Applicant"
}