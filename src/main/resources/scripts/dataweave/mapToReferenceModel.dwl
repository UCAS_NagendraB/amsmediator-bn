%dw 1.0
%output application/java
%var referenceTextForReferenceId = (referenceId) -> (flowVars.ReferenceTextData filter $.ReferenceId == referenceId reduce $).ReferenceText
%var deriveNameOfReferee = (title, firstname, lastname) -> ( //Fault tolerance for null titles.
    ((title ++ " " ) when title != null otherwise "")
    ++ substring(firstname,0,20) ++ " " ++ substring(lastname,0,20)
)
---
flowVars.ApplicationData.ReferenceRequests filter ($.CancelledDate == null 
	                                                  and $.RefereeDeclinedDate == null
	                                                  and $.RequestStatus != "0"
) map ((Reference) -> {
	ukpUreId : null,//pk set later
	ukpUapId : flowVars.application.ukpUapId,
	refereeAddress : using (RefereeAddress = Reference.PostalAddress) {
             address1: RefereeAddress.Line1,
             address2: RefereeAddress.Line2,
             address3: RefereeAddress.Line3,
             address4: RefereeAddress.Line4,
             postcode: RefereeAddress.Postcode,
             trfCtyId : lookup("dataLookupFlow",{
                 "returnColumn": "trf_cty_id",
                 "keyColumn": "ams_code",
                 "dataflowname": "getCountryData", 
                 "lookupValue": RefereeAddress.Country.Id}) default null when RefereeAddress.Country != null otherwise null,
             version: null
       } as :object {
             class: "com.ucas.ukpass.model.Address"
       },
	attached : null,
	attachmentExtension : null,
	emailAddress : Reference.EmailAddress,
	filename : null,
	nameOfReferee : trim deriveNameOfReferee(Reference.Title.Caption, Reference.Firstname, Reference.Lastname),
	organisation : Reference.Organisation,
	position : substring(Reference.JobTitle,0,20),
	principalRefereeFlag : "Y" when $$ == 0 otherwise "N", //First input Referee deemed principal, as per legacy
	directory : null,
	telephone : Reference.PhoneNumber,
	website : null,
//	lastUpdated : DONE VIA TRIGGER
//	versionNumber : DONE VIA TRIGGER
	reference : referenceTextForReferenceId(Reference.ReferenceId)
} as :object {
	class: "com.ucas.ukpass.model.Reference"
}
)
