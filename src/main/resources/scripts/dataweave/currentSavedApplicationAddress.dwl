%dw 1.0
%output application/java
---
{	
	agentAddress: {
		tpzAddId:  payload[0].TPZ_ADD_ID
	} as :object {
		class: "com.ucas.ukpass.model.Address"
	}
} as :object {
	class: "com.ucas.ukpass.model.Application"
}