package com.ucas.utils;

import java.util.Map;

public class ApplicationStatus {
	
	public static final String LEGACY_STATUS_DRAFT = "DRAFT";
	public static final String LEGACY_STATUS_REFERRED = "REFERRED";
	public static final String LEGACY_STATUS_REJECTED = "UNSUCCESS";
	public static final String LEGACY_STATUS_DECLINED = "DECLINED";
	public static final String LEGACY_STATUS_REJECTED_CONF = "REJATCONF";
	public static final String LEGACY_STATUS_WITHDRAWN = "WITHDRAWN";
	public static final String LEGACY_STATUS_INTERVIEW = "INTERVIEW";
	public static final String LEGACY_STATUS_CONDITIONAL_OFFER = "C OFFER";
	public static final String LEGACY_STATUS_UNCONDITIONAL_OFFER = "U OFFER";
	public static final String LEGACY_STATUS_CONDITIONAL_ACCEPT = "ACCEPT C";
	public static final String LEGACY_STATUS_UNCONDITIONAL_ACCEPT = "ACCEPT U";
	public static final String LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_NO_CHANGE = "ACCEPT UNC";
	public static final String LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE = "ACCEPT UYC";
	public static final String LEGACY_STATUS_ARCHIVED = "ARCHIVED";
	public static final String LEGACY_STATUS_CANCELLED = "CANCELLED";
	
	public static final String WITHDRAW_REASON_DEATH = "W9";
	
	
	public static String deriveStatus(java.util.Map<String,Object> application){

		String topazStatus = "";
		String withdrawReason = "";
		boolean isSameCourse = true;
	
		switch (((String)application.get("StatusCode")).toLowerCase()){
			case "draft":
				topazStatus = ApplicationStatus.LEGACY_STATUS_DRAFT;
				break;
			case "awaitingdecision":
			case "referred":
				topazStatus = ApplicationStatus.LEGACY_STATUS_REFERRED;
				break;
			case "interview":
				topazStatus = ApplicationStatus.LEGACY_STATUS_INTERVIEW;
				break;
			case "conditionaloffer":
				topazStatus = ApplicationStatus.LEGACY_STATUS_CONDITIONAL_OFFER;
				break;
			case "unconditionaloffer":
				topazStatus = ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER;
				break;
			case "acceptedconditionaloffer":
				topazStatus = ApplicationStatus.LEGACY_STATUS_CONDITIONAL_ACCEPT;
				break;
			case "acceptedunconditionaloffer":
				topazStatus = ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_ACCEPT;
				// It maps this way regardless of whether there has been a change to the course
				break;
			case "confirmedconditionaloffer":
				isSameCourse = ((String)((Map)application.get("CourseUnderConsideration")).get("CourseId")).equals( (String)((Map)((Map)application.get("Application")).get("Opportunity")).get("CourseId") );
				if(isSameCourse){
					topazStatus = ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_NO_CHANGE;
				}else{
					topazStatus = ApplicationStatus.LEGACY_STATUS_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE;
				}    
				break;
			case "rejected":
				topazStatus = ApplicationStatus.LEGACY_STATUS_REJECTED;
				break;
			case "rejectedacceptedconditional":
				topazStatus = ApplicationStatus.LEGACY_STATUS_REJECTED_CONF;
				break;
			case "declinedconditionaloffer":
			case "declinedunconditionaloffer":
				topazStatus = ApplicationStatus.LEGACY_STATUS_DECLINED;
				break;
			case "considerationwithdrawn":
			case "applicationwithdrawn":
				topazStatus = ApplicationStatus.LEGACY_STATUS_WITHDRAWN;
				break;
			case "applicationwithdrawnbyucas":
				withdrawReason = (String)((Map)((Map)application.get("UcasWithdrawalInformation")).get("WithdrawReason")).get("Id");
				if (withdrawReason.equals("10006")){
					// withdrawReason 10006 means "Death of applicant"
					topazStatus = ApplicationStatus.LEGACY_STATUS_CANCELLED;	
				}
				else if (withdrawReason.equals("10007")){
					// withdrawReason 10007 means "Fraudulent application"
					topazStatus = ApplicationStatus.LEGACY_STATUS_CANCELLED;	
				}
				else if (withdrawReason.equals("10008")){
					// withdrawReason 10008 means "Duplicate application"
					topazStatus = ApplicationStatus.LEGACY_STATUS_CANCELLED;	
				} 
				break;
			case "archived":
				topazStatus = ApplicationStatus.LEGACY_STATUS_ARCHIVED;
				break;				
			case "awaitingpaymentconclusion":
				topazStatus = ApplicationStatus.LEGACY_STATUS_DRAFT;
				break;		
			case "awaitingpaymentunlock":
				topazStatus = ApplicationStatus.LEGACY_STATUS_DRAFT;
				break;
			case "submissionstarted":
				topazStatus = ApplicationStatus.LEGACY_STATUS_DRAFT;
				break;
		}
	
		return topazStatus;
	}

}