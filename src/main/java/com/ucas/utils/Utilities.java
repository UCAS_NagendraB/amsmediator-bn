package com.ucas.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {

	public static final Date getDateFromISODateTimeString(String isoDateTimeString) {
		String dateNoTimeZone = isoDateTimeString.substring(0, isoDateTimeString.indexOf("Z"));

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		Date theDate = null;

		try {
			theDate = formatter.parse(dateNoTimeZone);
		} catch (ParseException e) {
		}
		
		return theDate;
	}
}
