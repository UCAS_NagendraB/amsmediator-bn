package com.ucas.utils;

import static net.javacrumbs.jsonunit.JsonAssert.*;
import static net.javacrumbs.jsonunit.core.Option.*;

public class FnJsonAssert {
	
	public static void Equals(String control, String compare)
	 {	
		assertJsonEquals(control, compare,when(IGNORING_ARRAY_ORDER,IGNORING_EXTRA_FIELDS));
	 }
}
