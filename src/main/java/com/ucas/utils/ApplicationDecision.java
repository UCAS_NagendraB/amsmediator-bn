package com.ucas.utils;

import java.util.Map;

public class ApplicationDecision {

	//'Referred' persisted as null not 'REF' 
	//ODL (Outstanding Decisions List) view selects where decision is null
	public static final String LEGACY_CODE_REFERRED = null;
	public static final String LEGACY_CODE_REJECTED = "REJ";
	public static final String LEGACY_CODE_REJECTED_CONF = "RYC";
	public static final String LEGACY_CODE_WITHDRAWN = "W";
	public static final String LEGACY_CODE_INTERVIEW = "INT";
	public static final String LEGACY_CODE_CONDITIONAL_OFFER = "COO";
	public static final String LEGACY_CODE_UNCONDITIONAL_OFFER = "UCO";
	public static final String LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_NO_CHANGE = "UNC";
	public static final String LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE = "UYC";

	public static String deriveCode(java.util.Map<String, Object> application) {

		String decisionType = (String) ((Map<?, ?>) application.get("Decision")).get("Type");
		String statusCode = (String) application.get("StatusCode");

		if (decisionType == null) {
			return ApplicationDecision.LEGACY_CODE_REFERRED;
		}

		switch (decisionType) {
			case "InterviewRequired":
				return ApplicationDecision.LEGACY_CODE_INTERVIEW;
			case "ConditionalOffer":
				boolean isConfirmationOffer = "confirmedconditionaloffer".equalsIgnoreCase(statusCode);
				boolean isDeclinedUnconditionalOffer = "declinedunconditionaloffer".equalsIgnoreCase(statusCode);

				if (isConfirmationOffer || isDeclinedUnconditionalOffer) {
					String courseUnderConsiderationOptionId = (String) ((Map<?, ?>) application.get("CourseUnderConsideration")).get("CourseOptionId");
					String opportunityOptionId = (String) ((Map<?, ?>) ((Map<?, ?>) application.get("Application")).get("Opportunity")).get("CourseOptionId");
					boolean courseHasChanged = !courseUnderConsiderationOptionId.equals(opportunityOptionId);
					
					if (courseHasChanged) {
						return ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_WITH_CHANGE;
					} else {
						return ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER_CONF_NO_CHANGE;
					}
				} else {
					return ApplicationDecision.LEGACY_CODE_CONDITIONAL_OFFER;
				}	
			case "UnconditionalOffer": {
				return ApplicationDecision.LEGACY_CODE_UNCONDITIONAL_OFFER;
			}
			case "Rejected": {
				boolean isConfirmationReject = "RejectedAcceptedConditional".equals(statusCode);
				
				if (isConfirmationReject) {
					return ApplicationDecision.LEGACY_CODE_REJECTED_CONF;
				} else {
					return ApplicationDecision.LEGACY_CODE_REJECTED;
				}
			}
			case "Withdrawn":
				return ApplicationDecision.LEGACY_CODE_WITHDRAWN;
			default:
				return "";
		}
	}
}
