package com.ucas.amsmediator.exception;

public class StatusNotDerivedException extends RuntimeException {
	
	public StatusNotDerivedException(String message) {
		super(message);
	}
}
