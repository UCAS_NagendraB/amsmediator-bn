package com.ucas.amsmediator.exception;

public class CourseNotFoundException extends RuntimeException {
	
	public CourseNotFoundException(String message) {
		super(message);
	}
}
