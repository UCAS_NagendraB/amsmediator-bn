package com.ucas.amsmediator;

public class Constants {
	public static final String PID_NOT_FOUND = "Personal id not found";
	public static final String SCHEME_CODE_NOT_FOUND = "App Scheme Code not found";
	public static final String PROCESS_MESSAGE = "Process Message";
	public static final String DISCARD_MESSAGE = "Discard Message";
	public static final String MESSAGE_TYPE_SUBMIT = "Message Type Submit";
	public static final String MESSAGE_TYPE_UPDATE = "Message Type Update";
	public static final String MESSAGE_TYPE_OTHER = "Message Type Other";
	public static final String APPLICANT_RECORD_NOT_FOUND = "APPLICANT_RECORD_NOT_FOUND";
	public static final String APPLICATION_RECORD_NOT_FOUND = "APPLICATION_RECORD_NOT_FOUND";
}
