package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class Application {

	private Long ukpUapId;
	private Long tpzAptId;
	private Long ukpUicId;
	private Address agentAddress;
	private Long ukpUcaId;
	private Long ukpUqlId;
	private Long trfAwrId;
	private Long trfDscId;
	private Long ukpUicIdPrevious;
	private Long ukpUicIdEnrolled;
	private Long tpzAdsId;
	private String applicationCode;
	private Character criminalConvictionFlag;
	private Date dateApnLastModified;
	private Date dateArchived;
	private Date dateToIns;
	private Date dateToTopaz;
	private Date dateSubmitted;
	private String decision;
	private String decisionOriginal;
	private String disSpecialNeedsText;
	private Date enrolledDate;
	private Character fundingEmployerFlag;
	private Character fundingOther;
	private Character fundingScholarship;
	private Character fundingSelf;
	private String fundingText;
	private String offerText;
	private String origination;
	private String response;
	private String responseOriginal;
	private Integer startMonth;
	private Integer startYear;
	private String status;
	private String statusOld;
	private Integer versionNumber;
	private Date lastUpdated;
	private Character agentUsed;
	private String agentCode;
	private String agentName;
	private String removedPersonalId;
	private String removedApplicationCode;
	private String decReason;
	private String agentEmailAddress;
	private String personalStatement;
	private Character underConsideration;
	private String personalStatementU;
	private String agentNameU;
	private String unicodeApprovalReq;
	private String unicodeApproval;
	private Character unicodeMsgShown;
	private String decReasonOld;

	public Long getUkpUapId() {
		return ukpUapId;
	}

	public void setUkpUapId(Long ukpUapId) {
		this.ukpUapId = ukpUapId;
	}

	public Long getTpzAptId() {
		return tpzAptId;
	}

	public void setTpzAptId(Long tpzAptId) {
		this.tpzAptId = tpzAptId;
	}

	public Long getUkpUicId() {
		return ukpUicId;
	}

	public void setUkpUicId(Long ukpUicId) {
		this.ukpUicId = ukpUicId;
	}

	public Address getAgentAddress() {
		return agentAddress;
	}

	public void setAgentAddress(Address agentAddress) {
		this.agentAddress = agentAddress;
	}

	public Long getUkpUcaId() {
		return ukpUcaId;
	}

	public void setUkpUcaId(Long ukpUcaId) {
		this.ukpUcaId = ukpUcaId;
	}

	public Long getUkpUqlId() {
		return ukpUqlId;
	}

	public void setUkpUqlId(Long ukpUqlId) {
		this.ukpUqlId = ukpUqlId;
	}

	public Long getTrfAwrId() {
		return trfAwrId;
	}

	public void setTrfAwrId(Long trfAwrId) {
		this.trfAwrId = trfAwrId;
	}

	public Long getTrfDscId() {
		return trfDscId;
	}

	public void setTrfDscId(Long trfDscId) {
		this.trfDscId = trfDscId;
	}

	public Long getUkpUicIdPrevious() {
		return ukpUicIdPrevious;
	}

	public void setUkpUicIdPrevious(Long ukpUicIdPrevious) {
		this.ukpUicIdPrevious = ukpUicIdPrevious;
	}

	public Long getUkpUicIdEnrolled() {
		return ukpUicIdEnrolled;
	}

	public void setUkpUicIdEnrolled(Long ukpUicIdEnrolled) {
		this.ukpUicIdEnrolled = ukpUicIdEnrolled;
	}

	public Long getTpzAdsId() {
		return tpzAdsId;
	}

	public void setTpzAdsId(Long tpzAdsId) {
		this.tpzAdsId = tpzAdsId;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public Character getCriminalConvictionFlag() {
		return criminalConvictionFlag;
	}
	
	public String getCriminalConvictionFlagAsString() {
		return (criminalConvictionFlag != null) ? String.valueOf(criminalConvictionFlag) : null;
	}

	public void setCriminalConvictionFlag(Character criminalConvictionFlag) {
		this.criminalConvictionFlag = criminalConvictionFlag;
	}

	public Date getDateApnLastModified() {
		return dateApnLastModified;
	}

	public void setDateApnLastModified(Date dateApnLastModified) {
		this.dateApnLastModified = dateApnLastModified;
	}

	public Date getDateArchived() {
		return dateArchived;
	}
	
	public Date getDateArchivedAsSqlDate() {
		return (dateArchived != null) ? new java.sql.Date(dateArchived.getTime()) : null;
	}
	
	public void setDateArchived(Date dateArchived) {
		this.dateArchived = dateArchived;
	}

	public Date getDateToIns() {
		return dateToIns;
	}

	public void setDateToIns(Date dateToIns) {
		this.dateToIns = dateToIns;
	}

	public Date getDateToTopaz() {
		return dateToTopaz;
	}

	public void setDateToTopaz(Date dateToTopaz) {
		this.dateToTopaz = dateToTopaz;
	}

	public Date getDateSubmitted() {
		return dateSubmitted;
	}

	public Date getDateSubmittedAsSqlDate() {
		return (dateSubmitted != null) ? new java.sql.Date(dateSubmitted.getTime()) : null;
	}
	
	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public String getDecisionOriginal() {
		return decisionOriginal;
	}

	public void setDecisionOriginal(String decisionOriginal) {
		this.decisionOriginal = decisionOriginal;
	}

	public String getDisSpecialNeedsText() {
		return disSpecialNeedsText;
	}

	public void setDisSpecialNeedsText(String disSpecialNeedsText) {
		this.disSpecialNeedsText = disSpecialNeedsText;
	}

	public Date getEnrolledDate() {
		return enrolledDate;
	}

	public Date getEnrolledDateAsSqlDate() {
		return (enrolledDate != null) ? new java.sql.Date(enrolledDate.getTime()) : null;
	}
	
	public void setEnrolledDate(Date enrolledDate) {
		this.enrolledDate = enrolledDate;
	}

	public Character getFundingEmployerFlag() {
		return fundingEmployerFlag;
	}
	
	public String getFundingEmployerFlagAsString() {
		return (fundingEmployerFlag != null) ? String.valueOf(fundingEmployerFlag) : null;
	}

	public void setFundingEmployerFlag(Character fundingEmployerFlag) {
		this.fundingEmployerFlag = fundingEmployerFlag;
	}

	public Character getFundingOther() {
		return fundingOther;
	}

	public String getFundingOtherAsString() {
		return (fundingOther != null) ? String.valueOf(fundingOther) : null;
	}
	
	public void setFundingOther(Character fundingOther) {
		this.fundingOther = fundingOther;
	}

	public Character getFundingScholarship() {
		return fundingScholarship;
	}

	public String getFundingScholarshipAsString() {
		return (fundingScholarship != null) ? String.valueOf(fundingScholarship) : null;
	}
	
	public void setFundingScholarship(Character fundingScholarship) {
		this.fundingScholarship = fundingScholarship;
	}

	public Character getFundingSelf() {
		return fundingSelf;
	}

	public String getFundingSelfAsString() {
		return (fundingSelf != null) ? String.valueOf(fundingSelf) : null;
	}
	
	public void setFundingSelf(Character fundingSelf) {
		this.fundingSelf = fundingSelf;
	}

	public String getFundingText() {
		return fundingText;
	}

	public void setFundingText(String fundingText) {
		this.fundingText = fundingText;
	}

	public String getOfferText() {
		return offerText;
	}

	public void setOfferText(String offerText) {
		this.offerText = offerText;
	}

	public String getOrigination() {
		return origination;
	}

	public void setOrigination(String origination) {
		this.origination = origination;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponseOriginal() {
		return responseOriginal;
	}

	public void setResponseOriginal(String responseOriginal) {
		this.responseOriginal = responseOriginal;
	}

	public Integer getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(Integer startMonth) {
		this.startMonth = startMonth;
	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusOld() {
		return statusOld;
	}

	public void setStatusOld(String statusOld) {
		this.statusOld = statusOld;
	}

	public Integer getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Character getAgentUsed() {
		return agentUsed;
	}

	public String getAgentUsedAsString() {
		return (agentUsed != null) ? String.valueOf(agentUsed) : null;
	}
	
	public void setAgentUsed(Character agentUsed) {
		this.agentUsed = agentUsed;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		if (agentName == null) {
			this.agentName = null;
			this.agentNameU = null;
		} else {
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(agentName,
					AppConstants.DB_UAP_AGENTNAME_TEXT_LIMIT);
			this.agentName = ecVO.getAsciiValue();
			this.agentNameU = ecVO.getUnicodeValue();
		}
	}

	public String getRemovedPersonalId() {
		return removedPersonalId;
	}

	public void setRemovedPersonalId(String removedPersonalId) {
		this.removedPersonalId = removedPersonalId;
	}

	public String getRemovedApplicationCode() {
		return removedApplicationCode;
	}

	public void setRemovedApplicationCode(String removedApplicationCode) {
		this.removedApplicationCode = removedApplicationCode;
	}

	public String getDecReason() {
		return decReason;
	}

	public void setDecReason(String decReason) {
		this.decReason = decReason;
	}

	public String getAgentEmailAddress() {
		return agentEmailAddress;
	}

	public void setAgentEmailAddress(String agentEmailAddress) {
		this.agentEmailAddress = agentEmailAddress;
	}

	public String getPersonalStatement() {
		return personalStatement;
	}

	public void setPersonalStatement(String personalStatement) {
		ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(personalStatement,
				AppConstants.DB_UAP_PERSONALSTATEMENT_TEXT_LIMIT);
		this.personalStatement = ecVO.getAsciiValue();
		this.personalStatementU = ecVO.getUnicodeValue();
	}

	public Character getUnderConsideration() {
		return underConsideration;
	}
	
	public String getUnderConsiderationAsString() {
		return (underConsideration != null) ? String.valueOf(underConsideration) : null;
	}
	
	public void setUnderConsideration(Character underConsideration) {
		this.underConsideration = underConsideration;
	}

	public String getPersonalStatementU() {
		return personalStatementU;
	}

	public String getAgentNameU() {
		return agentNameU;
	}

	public String getUnicodeApprovalReq() {
		return unicodeApprovalReq;
	}

	public void setUnicodeApprovalReq(String unicodeApprovalReq) {
		this.unicodeApprovalReq = unicodeApprovalReq;
	}

	public String getUnicodeApproval() {
		return unicodeApproval;
	}

	public void setUnicodeApproval(String unicodeApproval) {
		this.unicodeApproval = unicodeApproval;
	}

	public Character getUnicodeMsgShown() {
		return unicodeMsgShown;
	}
	
	public String getUnicodeMsgShownAsString() {
		return (unicodeMsgShown != null) ? String.valueOf(unicodeMsgShown) : null;
	}
	
	public void setUnicodeMsgShown(Character unicodeMsgShown) {
		this.unicodeMsgShown = unicodeMsgShown;
	}

	public String getDecReasonOld() {
		return decReasonOld;
	}

	public void setDecReasonOld(String decReasonOld) {
		this.decReasonOld = decReasonOld;
	}
}
