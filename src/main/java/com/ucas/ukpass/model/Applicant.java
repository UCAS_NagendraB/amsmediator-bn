package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class Applicant {

	private String btecRegNo;
	private Character crbCheckFlag;
	private Character criminalConvictionFlag;
	private Date dateOfBirth;
	private Date dateUKEntry;
	private String emailAddress;
	private String emailAddress2;
	private Character emailAddressVerifiedFlag;
	private Character emailOptInFlag;
	private String emergencyContactName;
	private String emergencyContactTelephone;
	private String forenames;
	private String homeTelephone;
	private String initials;
	private String language;
	private Character largeFontRequiredFlag;
	private Date lastUpdated;
	private String personalId;
	private String mobileTelephone;
	private Character nagtyFlag;
	private String nagtyType;
	private String nationalInsuranceNumber;
	private Integer noOfDependents;
	private String password;
	private String previousSurname;
	private String scottishCandidateNo;
	private Character sex;
	private String specialNeeds;
	private String surname;
	private Address correspondenceAddress;
	private Address homeAddress;
	private Long tpzAptId;
	private Long tpzAprId;
	private Long trfCtyIdBirth;
	private Long trfDscId;
	private Long trfEthId;
	private Long trfMrsId;
	private Long trfNatId;
	private Long trfNatIdDual;
	private Long trfProId;
	private Long trfRscId;
	private Long trfTtlId;
	private Integer version;
	private Long trfNidId1;
	private Long trfNidId2;
	private String username;
	//Custom oracle type - field not required for UKPASS?
	//public AblNameType ablName;
	private String emailVerification;
	private Character indefiniteLeaveUk;
	private Integer noOfAppsAllowed;
	private Character permanentHomeUk;
	private Character receiveInfoPost;
	private Character receiveInfoSms;
	private Integer applicationCodeSeq;
	private String preferredFirstName;
	private String passportNumber;
	private Date passportIssueDate;
	private Date passportExpiryDate;
	private String passportPlaceOfIssue;
	private String studentVisaRequired;
	private String ielts;
	private String toefl;
	private String forenamesU;
	private String initialsU;
	private String surnameU;
	private String preferredFirstNameU;
	private String previousSurnameU;
	private Character unicodeMsgShown;

	public String getBtecRegNo() {
		return btecRegNo;
	}

	public void setBtecRegNo(String btecRegNo) {
		this.btecRegNo = btecRegNo;
	}

	public Character getCrbCheckFlag() {
		return crbCheckFlag;
	}

	public String getCrbCheckFlagAsString() {
		return (crbCheckFlag != null) ? String.valueOf(crbCheckFlag) : null;
	}
	
	public void setCrbCheckFlag(Character crbCheckFlag) {
		this.crbCheckFlag = crbCheckFlag;
	}

	public Character getCriminalConvictionFlag() {
		return criminalConvictionFlag;
	}
	
	public String getCriminalConvictionFlagAsString() {
		return (criminalConvictionFlag != null) ? String.valueOf(criminalConvictionFlag) : null;
	}

	public void setCriminalConvictionFlag(Character criminalConvictionFlag) {
		this.criminalConvictionFlag = criminalConvictionFlag;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public Date getDateOfBirthAsSqlDate() {
		return (dateOfBirth != null) ? new java.sql.Date(dateOfBirth.getTime()) : null;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateUKEntry() {
		return dateUKEntry;
	}

	public Date getDateUKEntryAsSqlDate() {
		return (dateUKEntry != null) ? new java.sql.Date(dateUKEntry.getTime()) : null;
	}
	
	public void setDateUKEntry(Date dateUKEntry) {
		this.dateUKEntry = dateUKEntry;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress2() {
		return emailAddress2;
	}

	public void setEmailAddress2(String emailAddress2) {
		this.emailAddress2 = emailAddress2;
	}

	public Character getEmailAddressVerifiedFlag() {
		return emailAddressVerifiedFlag;
	}
	
	public String getEmailAddressVerifiedFlagAsString() {
		return (emailAddressVerifiedFlag != null) ? String.valueOf(emailAddressVerifiedFlag): null;
	}

	public void setEmailAddressVerifiedFlag(Character emailAddressVerifiedFlag) {
		this.emailAddressVerifiedFlag = emailAddressVerifiedFlag;
	}

	public Character getEmailOptInFlag() {
		return emailOptInFlag;
	}
	
	public String getEmailOptInFlagAsString() {
		return (emailOptInFlag != null) ? String.valueOf(emailOptInFlag): null ;
	}

	public void setEmailOptInFlag(Character emailOptInFlag) {
		this.emailOptInFlag = emailOptInFlag;
	}

	public String getEmergencyContactName() {
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName) {
		if(emergencyContactName != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(emergencyContactName,
				AppConstants.DB_APT_EMERGENCYCONTACTNAME_TEXT_LIMIT);
			this.emergencyContactName = ecVO.getAsciiValue();
		}		
	}

	public String getEmergencyContactTelephone() {
		return emergencyContactTelephone;
	}

	public void setEmergencyContactTelephone(String emergencyContactTelephone) {
		this.emergencyContactTelephone = emergencyContactTelephone;
	}

	public String getForenames() {
		return forenames;
	}

	public void setForenames(String forenames) {
		if(forenames != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(forenames,
				AppConstants.DB_APT_FORENAME_TEXT_LIMIT);
			this.forenames = ecVO.getAsciiValue();
			this.forenamesU = ecVO.getUnicodeValue();
		}
	}

	public String getHomeTelephone() {
		return homeTelephone;
	}

	public void setHomeTelephone(String homeTelephone) {
		this.homeTelephone = homeTelephone;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		if(initials != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(initials,
				AppConstants.DB_APT_INITIALS_TEXT_LIMIT);
			this.initials = ecVO.getAsciiValue();
			this.initialsU = ecVO.getUnicodeValue();
		}
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Character getLargeFontRequiredFlag() {
		return largeFontRequiredFlag;
	}
	
	public String getLargeFontRequiredFlagAsString() {
		return (largeFontRequiredFlag != null) ? String.valueOf(largeFontRequiredFlag) : null;
	}

	public void setLargeFontRequiredFlag(Character largeFontRequiredFlag) {
		this.largeFontRequiredFlag = largeFontRequiredFlag;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getMobileTelephone() {
		return mobileTelephone;
	}

	public void setMobileTelephone(String mobileTelephone) {
		this.mobileTelephone = mobileTelephone;
	}

	public Character getNagtyFlag() {
		return nagtyFlag;
	}

	public String getNagtyFlagAsString() {
		return (nagtyFlag != null) ? String.valueOf(nagtyFlag) : null;
	}
	
	public void setNagtyFlag(Character nagtyFlag) {
		this.nagtyFlag = nagtyFlag;
	}

	public String getNagtyType() {
		return nagtyType;
	}

	public void setNagtyType(String nagtyType) {
		this.nagtyType = nagtyType;
	}

	public String getNationalInsuranceNumber() {
		return nationalInsuranceNumber;
	}

	public void setNationalInsuranceNumber(String nationalInsuranceNumber) {
		this.nationalInsuranceNumber = nationalInsuranceNumber;
	}

	public Integer getNoOfDependents() {
		return noOfDependents;
	}

	public void setNoOfDependents(Integer noOfDependents) {
		this.noOfDependents = noOfDependents;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPreviousSurname() {
		return previousSurname;
	}

	public void setPreviousSurname(String previousSurname) {
		if(previousSurname != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(previousSurname,
				AppConstants.DB_APT_PREVSURNAME_TEXT_LIMIT);
			this.previousSurname = ecVO.getAsciiValue();
			this.previousSurnameU = ecVO.getUnicodeValue();
		}
	}

	public String getScottishCandidateNo() {
		return scottishCandidateNo;
	}

	public void setScottishCandidateNo(String scottishCandidateNo) {
		this.scottishCandidateNo = scottishCandidateNo;
	}

	public Character getSex() {
		return sex;
	}
	
	public String getSexAsString() {
		return (sex != null) ? String.valueOf(sex) : null;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public String getSpecialNeeds() {
		return specialNeeds;
	}

	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		if(surname != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(surname,
				AppConstants.DB_APT_SURNAME_TEXT_LIMIT);
			this.surname = ecVO.getAsciiValue();
			this.surnameU = ecVO.getUnicodeValue();
		}
	}

	public Address getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrespondenceAddress(Address correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}

	public Long getTpzAptId() {
		return tpzAptId;
	}

	public void setTpzAptId(Long tpzAptId) {
		this.tpzAptId = tpzAptId;
	}

	public Long getTpzAprId() {
		return tpzAprId;
	}

	public void setTpzAprId(Long tpzAprId) {
		this.tpzAprId = tpzAprId;
	}

	public Long getTrfCtyIdBirth() {
		return trfCtyIdBirth;
	}

	public void setTrfCtyIdBirth(Long trfCtyIdBirth) {
		this.trfCtyIdBirth = trfCtyIdBirth;
	}

	public Long getTrfDscId() {
		return trfDscId;
	}

	public void setTrfDscId(Long trfDscId) {
		this.trfDscId = trfDscId;
	}

	public Long getTrfEthId() {
		return trfEthId;
	}

	public void setTrfEthId(Long trfEthId) {
		this.trfEthId = trfEthId;
	}

	public Long getTrfMrsId() {
		return trfMrsId;
	}

	public void setTrfMrsId(Long trfMrsId) {
		this.trfMrsId = trfMrsId;
	}

	public Long getTrfNatId() {
		return trfNatId;
	}

	public void setTrfNatId(Long trfNatId) {
		this.trfNatId = trfNatId;
	}

	public Long getTrfNatIdDual() {
		return trfNatIdDual;
	}

	public void setTrfNatIdDual(Long trfNatIdDual) {
		this.trfNatIdDual = trfNatIdDual;
	}

	public Long getTrfProId() {
		return trfProId;
	}

	public void setTrfProId(Long trfProId) {
		this.trfProId = trfProId;
	}

	public Long getTrfRscId() {
		return trfRscId;
	}

	public void setTrfRscId(Long trfRscId) {
		this.trfRscId = trfRscId;
	}

	public Long getTrfTtlId() {
		return trfTtlId;
	}

	public void setTrfTtlId(Long trfTtlId) {
		this.trfTtlId = trfTtlId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getTrfNidId1() {
		return trfNidId1;
	}

	public void setTrfNidId1(Long trfNidId1) {
		this.trfNidId1 = trfNidId1;
	}

	public Long getTrfNidId2() {
		return trfNidId2;
	}

	public void setTrfNidId2(Long trfNidId2) {
		this.trfNidId2 = trfNidId2;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailVerification() {
		return emailVerification;
	}

	public void setEmailVerification(String emailVerification) {
		this.emailVerification = emailVerification;
	}

	public Character getIndefiniteLeaveUk() {
		return indefiniteLeaveUk;
	}
	
	public String getIndefiniteLeaveUkAsString() {
		return (indefiniteLeaveUk != null) ? String.valueOf(indefiniteLeaveUk) : null;
	}

	public void setIndefiniteLeaveUk(Character indefiniteLeaveUk) {
		this.indefiniteLeaveUk = indefiniteLeaveUk;
	}

	public Integer getNoOfAppsAllowed() {
		return noOfAppsAllowed;
	}

	public void setNoOfAppsAllowed(Integer noOfAppsAllowed) {
		this.noOfAppsAllowed = noOfAppsAllowed;
	}

	public Character getPermanentHomeUk() {
		return permanentHomeUk;
	}
	
	public String getPermanentHomeUkAsString() {
		return (permanentHomeUk != null) ? String.valueOf(permanentHomeUk) : null;
	}

	public void setPermanentHomeUk(Character permanentHomeUk) {
		this.permanentHomeUk = permanentHomeUk;
	}

	public Character getReceiveInfoPost() {
		return receiveInfoPost;
	}
	
	public String getReceiveInfoPostAsString() {
		return (receiveInfoPost != null) ? String.valueOf(receiveInfoPost) : null;
	}

	public void setReceiveInfoPost(Character receiveInfoPost) {
		this.receiveInfoPost = receiveInfoPost;
	}

	public Character getReceiveInfoSms() {
		return receiveInfoSms;
	}
	
	public String getReceiveInfoSmsAsString() {
		return (receiveInfoSms != null) ? String.valueOf(receiveInfoSms) : null;
	}

	public void setReceiveInfoSms(Character receiveInfoSms) {
		this.receiveInfoSms = receiveInfoSms;
	}

	public Integer getApplicationCodeSeq() {
		return applicationCodeSeq;
	}

	public void setApplicationCodeSeq(Integer applicationCodeSeq) {
		this.applicationCodeSeq = applicationCodeSeq;
	}

	public String getPreferredFirstName() {
		return preferredFirstName;
	}

	public void setPreferredFirstName(String preferredFirstName) {
		if(preferredFirstName != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(preferredFirstName,
				AppConstants.DB_APT_PREFFIRSTNAME_TEXT_LIMIT);
			this.preferredFirstName = ecVO.getAsciiValue();
			this.preferredFirstNameU = ecVO.getUnicodeValue();
		}
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Date getPassportIssueDate() {
		return passportIssueDate;
	}

	public Date getPassportIssueDateAsSqlDate() {
		return (passportIssueDate != null) ? new java.sql.Date(passportIssueDate.getTime()) : null;
	}
	
	public void setPassportIssueDate(Date passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public Date getPassportExpiryDateAsSqlDate() {
		return (passportExpiryDate != null) ? new java.sql.Date(passportExpiryDate.getTime()) : null;
	}
	
	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getPassportPlaceOfIssue() {
		return passportPlaceOfIssue;
	}

	public void setPassportPlaceOfIssue(String passportPlaceOfIssue) {
		this.passportPlaceOfIssue = passportPlaceOfIssue;
	}

	public String getStudentVisaRequired() {
		return studentVisaRequired;
	}

	public void setStudentVisaRequired(String studentVisaRequired) {
		this.studentVisaRequired = studentVisaRequired;
	}

	public String getIelts() {
		return ielts;
	}

	public void setIelts(String ielts) {
		this.ielts = ielts;
	}

	public String getToefl() {
		return toefl;
	}

	public void setToefl(String toefl) {
		this.toefl = toefl;
	}

	public String getForenamesU() {
		return forenamesU;
	}

	public String getInitialsU() {
		return initialsU;
	}

	public String getSurnameU() {
		return surnameU;
	}

	public String getPreferredFirstNameU() {
		return preferredFirstNameU;
	}
	
	public String getPreviousSurnameU() {
		return previousSurnameU;
	}

	public Character getUnicodeMsgShown() {
		return unicodeMsgShown;
	}
	
	public String getUnicodeMsgShownAsString() {
		return (unicodeMsgShown != null) ? String.valueOf(unicodeMsgShown) : null;
	}

	public void setUnicodeMsgShown(Character unicodeMsgShown) {
		this.unicodeMsgShown = unicodeMsgShown;
	}
}
