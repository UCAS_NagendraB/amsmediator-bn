package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class Address {

    private String primaryAddressFlag;
    private Date lastUpdated;
    private Long tpzAddId;
    private Long trfCtyId;
    private Integer version;
    private Integer mailsortCode;
    private String location;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String postcode;
    private String address1U;
    private String address2U;
    private String address3U;
    private String address4U;
    
    public String getPrimaryAddressFlag() {
        return primaryAddressFlag;
    }
    public void setPrimaryAddressFlag(String primaryAddressFlag) {
        this.primaryAddressFlag = primaryAddressFlag;
    }
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getTpzAddId() {
        return tpzAddId;
    }

    public void setTpzAddId(Long tpzAddId) {
        this.tpzAddId = tpzAddId;
    }

    public Long getTrfCtyId() {
        return trfCtyId;
    }

    public void setTrfCtyId(Long trfCtyId) {
        this.trfCtyId = trfCtyId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getMailsortCode() {
        return mailsortCode;
    }

    public void setMailsortCode(Integer mailsortCode) {
        this.mailsortCode = mailsortCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        if(address1 != null){
            ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(address1,
                AppConstants.DB_ADD_ADDRESSLINE_TEXT_LIMIT);
            this.address1 = ecVO.getAsciiValue();
            this.address1U = ecVO.getUnicodeValue();
        }
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        if(address2 != null){
            ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(address2,
                AppConstants.DB_ADD_ADDRESSLINE_TEXT_LIMIT);
            this.address2 = ecVO.getAsciiValue();
            this.address2U = ecVO.getUnicodeValue();
        }
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        if(address3 != null){
            ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(address3,
                AppConstants.DB_ADD_ADDRESSLINE_TEXT_LIMIT);
            this.address3 = ecVO.getAsciiValue();
            this.address3U = ecVO.getUnicodeValue();
        }
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        if(address4 != null){
            ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(address4,
                AppConstants.DB_ADD_ADDRESSLINE_TEXT_LIMIT);
            this.address4 = ecVO.getAsciiValue();
            this.address4U = ecVO.getUnicodeValue();
        }
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAddress1U() {
        return address1U;
    }

    public String getAddress2U() {
        return address2U;
    }

    public String getAddress3U() {
        return address3U;
    }

    public String getAddress4U() {
        return address4U;
    }
        
    @Override
    public boolean equals(Object obj) {
        Address other = (Address)obj;
        if (other == null) {
            // This address being compared to null
            return false;
        } else {
            // This address being compared to an actual address
            return (this.getAddress1().equals(other.getAddress1()) &&
                    this.getAddress2().equals(other.getAddress2()) &&
                    this.getAddress3().equals(other.getAddress3()) &&
                    this.getAddress4().equals(other.getAddress4()) &&
                    this.getPostcode().equals(other.getPostcode()));
        }
    }
}
