package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class WorkExperience {

	private Long ukpUweId;
	private Long tpzAptId;
	private Address experienceAddress;
	private Long trfCtyId;
	private String name;
	private String telephone;
	private String jobDescription;
	private Integer startYear;
	private Integer startMonth;
	private Integer endYear;
	private Integer endMonth;
	private Character workType;
	private Character voluntaryFlag;
	private Character noWorkExperienceFlag;
	private Date lastUpdated;
	private Character finishDateUnknown;
	private String nameU;
	private String jobDescriptionU;

	public Long getUkpUweId() {
		return ukpUweId;
	}

	public void setUkpUweId(Long ukpUweId) {
		this.ukpUweId = ukpUweId;
	}

	public Long getTpzAptId() {
		return tpzAptId;
	}

	public void setTpzAptId(Long tpzAptId) {
		this.tpzAptId = tpzAptId;
	}

	public Address getExperienceAddress() {
		return experienceAddress;
	}

	public void setExperienceAddress(Address experienceAddress) {
		this.experienceAddress = experienceAddress;
	}

	public Long getTrfCtyId() {
		return trfCtyId;
	}

	public void setTrfCtyId(Long trfCtyId) {
		this.trfCtyId = trfCtyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(name,
				AppConstants.DB_UWE_NAME_TEXT_LIMIT);
			this.name = ecVO.getAsciiValue();
			this.nameU = ecVO.getUnicodeValue();
		}
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		if(jobDescription != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(jobDescription,
				AppConstants.DB_UWE_JOBDESCRIPTION_TEXT_LIMIT);
			this.jobDescription = ecVO.getAsciiValue();
			this.jobDescriptionU = ecVO.getUnicodeValue();
		}
	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}

	public Integer getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(Integer startMonth) {
		this.startMonth = startMonth;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public Integer getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(Integer endMonth) {
		this.endMonth = endMonth;
	}

	public Character getWorkType() {
		return workType;
	}
	
	public String getWorkTypeAsString() {
		return (workType != null) ? String.valueOf(workType) : null;
	}

	public void setWorkType(Character workType) {
		this.workType = workType;
	}

	public Character getVoluntaryFlag() {
		return voluntaryFlag;
	}
	
	public String getVoluntaryFlagAsString() {
		return (voluntaryFlag != null) ? String.valueOf(voluntaryFlag) : null;
	}

	public void setVoluntaryFlag(Character voluntaryFlag) {
		this.voluntaryFlag = voluntaryFlag;
	}

	public Character getNoWorkExperienceFlag() {
		return noWorkExperienceFlag;
	}
	
	public String getNoWorkExperienceFlagAsString() {
		return (noWorkExperienceFlag != null) ? String.valueOf(noWorkExperienceFlag) : null;
	}

	public void setNoWorkExperienceFlag(Character noWorkExperienceFlag) {
		this.noWorkExperienceFlag = noWorkExperienceFlag;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Character getFinishDateUnknown() {
		return finishDateUnknown;
	}
	
	public String getFinishDateUnknownAsString() {
		return (finishDateUnknown != null) ? String.valueOf(finishDateUnknown) : null;
	}

	public void setFinishDateUnknown(Character finishDateUnknown) {
		this.finishDateUnknown = finishDateUnknown;
	}

	public String getNameU() {
		return nameU;
	}

	public void setNameU(String nameU) {
		this.nameU = nameU;
	}

	public String getJobDescriptionU() {
		return jobDescriptionU;
	}
}
