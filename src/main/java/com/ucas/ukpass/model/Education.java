package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class Education {

	private Character noFormalQualificationsFlag;
	private Character resultsAwaitedFlag;
	private Date finishDate;
	private Date startDate;
	private Date lastUpdated;
	private Short universityOrder;
	private Long nupInsId;
	private Long trfCtyId;
	private Long tpzSchId;
	private Long tpzAddId;
	private Long ukpUetId;
	private Long ukpUedId;
	private Long tpzAptId;
	private Integer dateTakenMonth;
	private Integer dateTakenYear;
	private Integer version;
	private String candidateNumber;
	private String additionalInformation;
	private String department;
	private String establishmentName;
	private String details;
	private String title;
	private String attendance;
	private String grade;
	private String subject;
	private String awardSubject;
	private String dissertationResearch;
	private String awardLevel;
	private String language;
	private String awardingBody;

	public Character getNoFormalQualificationsFlag() {
		return noFormalQualificationsFlag;
	}

	public String getNoFormalQualificationsFlagAsString() {
		return (noFormalQualificationsFlag != null) ? String.valueOf(noFormalQualificationsFlag) : null;
	}
	
	public void setNoFormalQualificationsFlag(Character noFormalQualificationsFlag) {
		this.noFormalQualificationsFlag = noFormalQualificationsFlag;
	}

	public Character getResultsAwaitedFlag() {
		return resultsAwaitedFlag;
	}

	public String getResultsAwaitedFlagAsString() {
		return (resultsAwaitedFlag != null) ? String.valueOf(resultsAwaitedFlag) : null;
	}
	
	public void setResultsAwaitedFlag(Character resultsAwaitedFlag) {
		this.resultsAwaitedFlag = resultsAwaitedFlag;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public Date getFinishDateAsSqlDate() {
		return (finishDate != null) ? new java.sql.Date(finishDate.getTime()) : null;
	}
	
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public Date getStartDate() {
		return startDate;
	}
	
	public Date getStartDateAsSqlDate() {
		return (startDate != null) ? new java.sql.Date(startDate.getTime()) : null;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Short getUniversityOrder() {
		return universityOrder;
	}

	public void setUniversityOrder(Short universityOrder) {
		this.universityOrder = universityOrder;
	}

	public Long getNupInsId() {
		return nupInsId;
	}

	public void setNupInsId(Long nupInsId) {
		this.nupInsId = nupInsId;
	}

	public Long getTrfCtyId() {
		return trfCtyId;
	}

	public void setTrfCtyId(Long trfCtyId) {
		this.trfCtyId = trfCtyId;
	}

	public Long getTpzSchId() {
		return tpzSchId;
	}

	public void setTpzSchId(Long tpzSchId) {
		this.tpzSchId = tpzSchId;
	}

	public Long getTpzAddId() {
		return tpzAddId;
	}

	public void setTpzAddId(Long tpzAddId) {
		this.tpzAddId = tpzAddId;
	}

	public Long getUkpUetId() {
		return ukpUetId;
	}

	public void setUkpUetId(Long ukpUetId) {
		this.ukpUetId = ukpUetId;
	}

	public Long getUkpUedId() {
		return ukpUedId;
	}

	public void setUkpUedId(Long ukpUedId) {
		this.ukpUedId = ukpUedId;
	}

	public Long getTpzAptId() {
		return tpzAptId;
	}

	public void setTpzAptId(Long tpzAptId) {
		this.tpzAptId = tpzAptId;
	}

	public Integer getDateTakenMonth() {
		return dateTakenMonth;
	}

	public void setDateTakenMonth(Integer dateTakenMonth) {
		this.dateTakenMonth = dateTakenMonth;
	}

	public Integer getDateTakenYear() {
		return dateTakenYear;
	}

	public void setDateTakenYear(Integer dateTakenYear) {
		this.dateTakenYear = dateTakenYear;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getCandidateNumber() {
		return candidateNumber;
	}

	public void setCandidateNumber(String candidateNumber) {
		this.candidateNumber = candidateNumber;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		if(additionalInformation != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(additionalInformation,
				AppConstants.DB_UED_ADDITIONALINFORMATION_TEXT_LIMIT);
			this.additionalInformation = ecVO.getAsciiValue();
		}
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEstablishmentName() {
		return establishmentName;
	}

	public void setEstablishmentName(String establishmentName) {
		if(establishmentName != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(establishmentName,
				AppConstants.DB_UED_ESTABLISHMENTNAME_TEXT_LIMIT);
			this.establishmentName = ecVO.getAsciiValue();
		}
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		if(title != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(title,
				AppConstants.DB_UED_TITLE_TEXT_LIMIT);
			this.title = ecVO.getAsciiValue();
		}
	}

	public String getAttendance() {
		return attendance;
	}

	public void setAttendance(String attendance) {
		this.attendance = attendance;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		if(subject != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(subject,
				AppConstants.DB_UED_SUBJECT_TEXT_LIMIT);
			this.subject = ecVO.getAsciiValue();
		}
	}

	public String getAwardSubject() {
		return awardSubject;
	}

	public void setAwardSubject(String awardSubject) {
		if(awardSubject != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(awardSubject,
				AppConstants.DB_UED_AWARDSUBJECT_TEXT_LIMIT);
			this.awardSubject = ecVO.getAsciiValue();
		}
	}

	public String getDissertationResearch() {
		return dissertationResearch;
	}

	public void setDissertationResearch(String dissertationResearch) {
		this.dissertationResearch = dissertationResearch;
	}

	public String getAwardLevel() {
		return awardLevel;
	}

	public void setAwardLevel(String awardLevel) {
		this.awardLevel = awardLevel;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getAwardingBody() {
		return awardingBody;
	}

	public void setAwardingBody(String awardingBody) {
		if(awardingBody != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(awardingBody,
				AppConstants.DB_UED_AWARDINGBODY_TEXT_LIMIT);
			this.awardingBody = ecVO.getAsciiValue();
		}
	}
}
