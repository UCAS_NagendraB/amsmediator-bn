package com.ucas.ukpass.model;

import java.util.Date;

import com.ucas.ukpass.common.AppConstants;
import com.ucas.ukpass.common.ExtendedCharacterUtils;
import com.ucas.ukpass.common.vo.ExtendedCharacterVO;

public class Reference {

	private Long ukpUreId;
	private Long ukpUapId;
	private Address refereeAddress;
	private Character attached;
	private String attachmentExtension;
	private String emailAddress;
	private String filename;
	private String nameOfReferee;
	private String organisation;
	private String position;
	private Character principalRefereeFlag;
	private String directory;
	private String telephone;
	private String website;
	private Date lastUpdated;
	private Integer versionNumber;
	private String reference;
	private String nameOfRefereeU;
	private String organisationU;
	private String positionU;
	private String referenceU;

	public Long getUkpUreId() {
		return ukpUreId;
	}

	public void setUkpUreId(Long ukpUreId) {
		this.ukpUreId = ukpUreId;
	}

	public Long getUkpUapId() {
		return ukpUapId;
	}

	public void setUkpUapId(Long ukpUapId) {
		this.ukpUapId = ukpUapId;
	}

	public Address getRefereeAddress() {
		return refereeAddress;
	}

	public void setRefereeAddress(Address refereeAddress) {
		this.refereeAddress = refereeAddress;
	}

	public Character getAttached() {
		return attached;
	}

	public String getAttachedAsString() {
		return (attached != null) ? String.valueOf(attached) : null;
	}
	
	public void setAttached(Character attached) {
		this.attached = attached;
	}

	public String getAttachmentExtension() {
		return attachmentExtension;
	}

	public void setAttachmentExtension(String attachmentExtension) {
		this.attachmentExtension = attachmentExtension;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getNameOfReferee() {
		return nameOfReferee;
	}

	public void setNameOfReferee(String nameOfReferee) {
		if(nameOfReferee != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(nameOfReferee,
				AppConstants.DB_URE_REFREENAME_TEXT_LIMIT);
			this.nameOfReferee = ecVO.getAsciiValue();
			this.nameOfRefereeU = ecVO.getUnicodeValue();
		}
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		if(organisation != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(organisation,
				AppConstants.DB_URE_ORGANISATION_TEXT_LIMIT);
			this.organisation = ecVO.getAsciiValue();
			this.organisationU = ecVO.getUnicodeValue();
		}
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		if(position != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(position,
				AppConstants.DB_URE_POSITION_TEXT_LIMIT);
			this.position = ecVO.getAsciiValue();
			this.positionU = ecVO.getUnicodeValue();
		}
	}

	public Character getPrincipalRefereeFlag() {
		return principalRefereeFlag;
	}

	public String getPrincipalRefereeFlagAsString() {
		return (principalRefereeFlag != null) ? String.valueOf(principalRefereeFlag) : null;
	}
	
	public void setPrincipalRefereeFlag(Character principalRefereeFlag) {
		this.principalRefereeFlag = principalRefereeFlag;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Integer getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		if(reference != null){
			ExtendedCharacterVO ecVO = ExtendedCharacterUtils.transformToVO(reference,
				AppConstants.DB_URE_REFERENCE_TEXT_LIMIT);
			this.reference = ecVO.getAsciiValue();
			this.referenceU = ecVO.getUnicodeValue();
		}
	}

	public String getNameOfRefereeU() {
		return nameOfRefereeU;
	}

	public String getOrganisationU() {
		return organisationU;
	}

	public String getPositionU() {
		return positionU;
	}

	public String getReferenceU() {
		return referenceU;
	}
}
