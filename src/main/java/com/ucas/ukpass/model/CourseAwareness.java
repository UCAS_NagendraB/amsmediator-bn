package com.ucas.ukpass.model;

import java.util.Date;

public class CourseAwareness {

	private Date lastUpdated;
	private Long ukpUcaId;
	private Integer version;
	private String code;
	private String description;
	
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public Long getUkpUcaId() {
		return ukpUcaId;
	}
	public void setUkpUcaId(Long ukpUcaId) {
		this.ukpUcaId = ukpUcaId;
	}
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
