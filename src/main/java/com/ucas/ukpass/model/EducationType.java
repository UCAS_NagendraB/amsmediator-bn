package com.ucas.ukpass.model;

import java.util.Date;

public class EducationType {

	private Date lastUpdated;
	private Long ukpUetId;
	private Integer version;
	private String description;
	private String code;

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Long getUkpUetId() {
		return ukpUetId;
	}

	public void setUkpUetId(Long ukpUetId) {
		this.ukpUetId = ukpUetId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
