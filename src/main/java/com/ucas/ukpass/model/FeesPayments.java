package com.ucas.ukpass.model;

import java.util.Date;

public class FeesPayments {

	private Character transactionType;
	private Character refundStatusFlag;
	private Date paymentDate;
	private Date refundDueDate;
	private Date lastUpdated;
	private Date refundMadeDate;
	private Long ukpUapId;
	private Long ukpUfpId;
	private Long trfPymId;
	private Long trfRfrId;
	private Integer version;
	private Float refundAmount;
	private Float amountPaid;
	private Float administrationFee;
	private Float applicationFee;
	private String datacashTransRefNo;
	private String details;

	public Character getTransactionType() {
		return transactionType;
	}
	
	public String getTransactionTypeAsString() {
		return (transactionType != null) ? String.valueOf(transactionType) : null;
	}

	public void setTransactionType(Character transactionType) {
		this.transactionType = transactionType;
	}

	public Character getRefundStatusFlag() {
		return refundStatusFlag;
	}

	public void setRefundStatusFlag(Character refundStatusFlag) {
		this.refundStatusFlag = refundStatusFlag;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public Date getPaymentDateAsSqlDate() {
		return (paymentDate != null) ? new java.sql.Date(paymentDate.getTime()) : null;
	}
	
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getRefundDueDate() {
		return refundDueDate;
	}

	public Date getRefundDueDateAsSqlDate() {
		return (refundDueDate != null) ? new java.sql.Date(refundDueDate.getTime()) : null;
	}
	
	public void setRefundDueDate(Date refundDueDate) {
		this.refundDueDate = refundDueDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getRefundMadeDate() {
		return refundMadeDate;
	}

	public Date getRefundMadeDateAsSqlDate() {
		return (refundMadeDate != null) ? new java.sql.Date(refundMadeDate.getTime()) : null;
	}
	
	public void setRefundMadeDate(Date refundMadeDate) {
		this.refundMadeDate = refundMadeDate;
	}

	public Long getUkpUapId() {
		return ukpUapId;
	}

	public void setUkpUapId(Long ukpUapId) {
		this.ukpUapId = ukpUapId;
	}

	public Long getUkpUfpId() {
		return ukpUfpId;
	}

	public void setUkpUfpId(Long ukpUfpId) {
		this.ukpUfpId = ukpUfpId;
	}

	public Long getTrfPymId() {
		return trfPymId;
	}

	public void setTrfPymId(Long trfPymId) {
		this.trfPymId = trfPymId;
	}

	public Long getTrfRfrId() {
		return trfRfrId;
	}

	public void setTrfRfrId(Long trfRfrId) {
		this.trfRfrId = trfRfrId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Float getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Float refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Float getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Float amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Float getAdministrationFee() {
		return administrationFee;
	}

	public void setAdministrationFee(Float administrationFee) {
		this.administrationFee = administrationFee;
	}

	public Float getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(Float applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getDatacashTransRefNo() {
		return datacashTransRefNo;
	}

	public void setDatacashTransRefNo(String datacashTransRefNo) {
		this.datacashTransRefNo = datacashTransRefNo;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
