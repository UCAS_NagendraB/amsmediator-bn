package com.ucas.ukpass.model;

import java.util.Date;

public class QuestionsAnswers {

	private Character separateCover;
	private Character attachmentAllowedFlag;
	private Character attachmentViewed;
	private Character requiredFlag;
	private Date lastUpdated;
	private Date attachmentUploadPrevDate;
	private Date attachmentUploadDate;
	private Long ukpUcqId;
	private Long ukpUiqId;
	private Long ukpUqaId;
	private Long ukpUapId;
	private Address answersAddress;
	private Integer position;
	private Integer version;
	private String answer;
	private String filename;
	private String directory;
	private String attachmentExtension;
	private String questionText;

	public Character getSeparateCover() {
		return separateCover;
	}

	public void setSeparateCover(Character separateCover) {
		this.separateCover = separateCover;
	}

	public String getAttachmentAllowedFlagAsString() {
		return (attachmentAllowedFlag != null) ? String.valueOf(attachmentAllowedFlag) : null;
	}

	public Character getAttachmentAllowedFlag() {
		return attachmentAllowedFlag;
	}

	public void setAttachmentAllowedFlag(Character attachmentAllowedFlag) {
		this.attachmentAllowedFlag = attachmentAllowedFlag;
	}

	public Character getAttachmentViewed() {
		return attachmentViewed;
	}

	public void setAttachmentViewed(Character attachmentViewed) {
		this.attachmentViewed = attachmentViewed;
	}

	public Character getRequiredFlag() {
		return requiredFlag;
	}

	public void setRequiredFlag(Character requiredFlag) {
		this.requiredFlag = requiredFlag;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getAttachmentUploadPrevDate() {
		return attachmentUploadPrevDate;
	}

	public Date getAttachmentUploadPrevDateAsSqlDate() {
		return (attachmentUploadPrevDate != null) ? new java.sql.Date(attachmentUploadPrevDate.getTime()) : null;
	}
	
	public void setAttachmentUploadPrevDate(Date attachmentUploadPrevDate) {
		this.attachmentUploadPrevDate = attachmentUploadPrevDate;
	}

	public Date getAttachmentUploadDate() {
		return attachmentUploadDate;
	}

	public Date getAttachmentUploadDateAsSqlDate() {
		return (attachmentUploadDate != null) ? new java.sql.Date(attachmentUploadDate.getTime()) : null;
	}
	
	public void setAttachmentUploadDate(Date attachmentUploadDate) {
		this.attachmentUploadDate = attachmentUploadDate;
	}

	public Long getUkpUcqId() {
		return ukpUcqId;
	}

	public void setUkpUcqId(Long ukpUcqId) {
		this.ukpUcqId = ukpUcqId;
	}

	public Long getUkpUiqId() {
		return ukpUiqId;
	}

	public void setUkpUiqId(Long ukpUiqId) {
		this.ukpUiqId = ukpUiqId;
	}

	public Long getUkpUqaId() {
		return ukpUqaId;
	}

	public void setUkpUqaId(Long ukpUqaId) {
		this.ukpUqaId = ukpUqaId;
	}

	public Long getUkpUapId() {
		return ukpUapId;
	}

	public void setUkpUapId(Long ukpUapId) {
		this.ukpUapId = ukpUapId;
	}

	public Address getAnswersAddress() {
		return answersAddress;
	}

	public void setAnswersAddress(Address answersAddress) {
		this.answersAddress = answersAddress;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getAttachmentExtension() {
		return attachmentExtension;
	}

	public void setAttachmentExtension(String attachmentExtension) {
		this.attachmentExtension = attachmentExtension;
	}

	public String getQuestionText() {
		return questionText;
	}
	
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
}
