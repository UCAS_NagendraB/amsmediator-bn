package com.ucas.ukpass.common;

public class AppConstants {
	public static final int DB_ADD_ADDRESSLINE_TEXT_LIMIT = 50;
	public static final int DB_APT_INITIALS_TEXT_LIMIT = 3;
	public static final int DB_APT_FORENAME_TEXT_LIMIT = 50;
	public static final int DB_APT_SURNAME_TEXT_LIMIT = 30;
	public static final int DB_APT_PREFFIRSTNAME_TEXT_LIMIT = 35;
	public static final int DB_APT_PREVSURNAME_TEXT_LIMIT = 30;
	public static final int DB_APT_EMERGENCYCONTACTNAME_TEXT_LIMIT = 50;
	public static final int DB_UAP_PERSONALSTATEMENT_TEXT_LIMIT = 6000;
	public static final int DB_UAP_AGENTNAME_TEXT_LIMIT = 250;
	public static final int DB_UED_AWARDINGBODY_TEXT_LIMIT = 50;
	public static final int DB_UED_ADDITIONALINFORMATION_TEXT_LIMIT = 100; 	
	public static final int DB_UED_AWARDSUBJECT_TEXT_LIMIT = 200;
	public static final int DB_UED_ESTABLISHMENTNAME_TEXT_LIMIT = 100; 
	public static final int DB_UED_SUBJECT_TEXT_LIMIT = 200;
	public static final int DB_UED_TITLE_TEXT_LIMIT = 100;
	public static final int DB_URE_REFREENAME_TEXT_LIMIT = 50;
	public static final int DB_URE_ORGANISATION_TEXT_LIMIT = 250;
	public static final int DB_URE_POSITION_TEXT_LIMIT = 20;
	public static final int DB_URE_REFERENCE_TEXT_LIMIT = 4000;
	public static final int DB_UWE_NAME_TEXT_LIMIT = 100;
	public static final int DB_UWE_JOBDESCRIPTION_TEXT_LIMIT = 500;
}
