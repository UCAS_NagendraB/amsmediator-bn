package com.ucas.ukpass.common.vo;

public class ExtendedCharacterVO {

	private String asciiValue;
	private String unicodeValue;

	public ExtendedCharacterVO(String asciiValue, String unicodeValue) {
		this.asciiValue = asciiValue;
		this.unicodeValue = unicodeValue;
	}

	public String getAsciiValue() {
		return asciiValue;
	}

	public void setAsciiValue(String asciiValue) {
		this.asciiValue = asciiValue;
	}

	public String getUnicodeValue() {
		return unicodeValue;
	}

	public void setUnicodeValue(String unicodeValue) {
		this.unicodeValue = unicodeValue;
	}
}
