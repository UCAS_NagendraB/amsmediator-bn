package com.ucas.ukpass.common;

import com.ucas.ukpass.common.vo.ExtendedCharacterVO;
import com.ucas.util.AsciiUtility;

public class ExtendedCharacterUtils {
	
	public static ExtendedCharacterVO transformToVO(String inString, final int maxFieldSize) {
		String unicodeValue = null;
		String asciiValue = null;
		
		if(AsciiUtility.getInstance().containsNotBasicLatin(inString)) {
			unicodeValue = inString;
			asciiValue = AsciiUtility.getInstance().convertToAscii(inString);
			
			if(asciiValue.length() > maxFieldSize) {
				asciiValue = asciiValue.substring(0, maxFieldSize);
			}
		} else {
			asciiValue = inString;
		}
		
		return new ExtendedCharacterVO(asciiValue, unicodeValue);
	}
}
